package main

import (
  "fmt"
  "net"
  "time"
)

const (
  cmdPing     = '?'
  cmdVersion  = 'V'
  cmdMCPRead  = 'i'
  cmdMCPWrite = 'I'
  cmdMAXRead  = 'r'
  cmdMAXWrite = 'R'
  cmdPinGet   = ';'
  cmdPinSet   = ':'

  cmdDebugTimeout = 'T'

  rspShort  = '|'
  rspLong   = '}'
  rspNone   = '~'
  rspErr    = '!'

  errNotImplemented = 0x28
  errBadRequest     = 0x30

  commsProtocolVersion  = 0x01
)

var (
  pinState    map[byte]bool
  mcpMemory   []uint16
  maxMemory   []uint8
)

func handleMessage(conn *net.UDPConn, incoming []byte, addr net.Addr) {
  response := make([]byte, 259)
  response[0] = incoming[0] /* sequence */

  switch incoming[1] {
    case cmdPing:
      fmt.Printf("%d: ping: %d\n", incoming[0], incoming[2])
      response[1] = rspNone
      response[2] = incoming[2]
      conn.WriteTo(response[:3], addr)
      return

    case cmdVersion:
      fmt.Printf("%d: version\n", incoming[0])
      response[1] = rspNone
      response[2] = commsProtocolVersion
      conn.WriteTo(response[:3], addr)
      return

    case cmdMCPRead:
      value := mcpMemory[incoming[2] & 7]
      fmt.Printf("%d: MCP3008 read: %d (=> %d)\n",
          incoming[0], incoming[2], value)
      response[1] = rspLong
      response[2] = 1
      response[3] = uint8(value >> 8) & 3
      response[4] = uint8(value) & 255
      conn.WriteTo(response[:5], addr)
      return

    case cmdMCPWrite:
      mcpMemory[incoming[2] & 7] = uint16(
          uint16(incoming[3] & 3) << 8 | uint16(incoming[4]))
      fmt.Printf("%d: MCP3008 write: %d <= %d\n",
          incoming[0], incoming[2], mcpMemory[incoming[2] & 7])
      response[1] = rspNone
      response[2] = 1
      conn.WriteTo(response[:3], addr)
      return

    case cmdMAXRead:
      if (incoming[2] + incoming[3]) > 0x6F {
        fmt.Printf("%d: MAX6955 read from %x length %d (out of bounds)\n",
            incoming[0], incoming[2], incoming[3])
        response[1] = rspErr
        response[2] = errBadRequest
        conn.WriteTo(response[:3], addr)
        return
      }

      response[1] = rspShort
      response[2] = incoming[3]
      for i := byte(0); i < incoming[3]; i++ {
        offset := incoming[2] + i
        response[i + 3] = maxMemory[offset]
      }
      fmt.Printf("%d: MAX6955 read from %x length %d %v\n",
          incoming[0], incoming[2], incoming[3], response[3:3 + incoming[3]])
      conn.WriteTo(response[:incoming[3] + 3], addr)
      return

    case cmdMAXWrite:
      if (incoming[2] + incoming[3]) > 0x6F {
        fmt.Printf("%d: MAX6955 write from %x length %d (out of bounds)\n",
            incoming[0], incoming[2], incoming[3])
        response[1] = rspErr
        response[2] = errBadRequest
        conn.WriteTo(response[:3], addr)
        return
      }

      for i := byte(0); i < incoming[3]; i++ {
        target := i + incoming[2]
        if target >= 0xD && target <= 0xF {
          return
        } else if target >= 0x60 {
          maxMemory[target - 0x40] = incoming[i + 4]
          maxMemory[target - 0x20] = incoming[i + 4]
        } else {
          maxMemory[target] = incoming[i + 4]
        }
      }

      fmt.Printf("%d: MAX6955 write from %x length %d %v\n",
          incoming[0], incoming[2], incoming[3], incoming[4:])
      response[1] = rspNone
      response[2] = incoming[3]
      conn.WriteTo(response[:3], addr)
      return

    case cmdPinGet:
      fmt.Printf("%d: pin get: %d => %v\n",
          incoming[0], incoming[2], pinState[incoming[2]])
      response[1] = rspShort
      if pinState[incoming[2]] {
        response[2] = 1
      } else {
        response[2] = 0
      }
      conn.WriteTo(response[:3], addr)
      return

    case cmdPinSet:
      fmt.Printf("%d: pin set: %d <= %d\n",
          incoming[0], incoming[2], incoming[3])
      switch incoming[2] {
        case 17: fallthrough
        case 22: fallthrough
        case 27:
          if incoming[3] == 1 {
            pinState[incoming[2]] = true
          } else if incoming[3] == 0 {
            pinState[incoming[2]] = false
          } else {
            response[1] = rspErr
            response[2] = errBadRequest
            conn.WriteTo(response[:3], addr)
            return
          }
          response[1] = rspNone
          response[2] = incoming[3]
          conn.WriteTo(response[:3], addr)
          return

        default:
          response[1] = rspErr
          response[2] = errBadRequest
          conn.WriteTo(response[:3], addr)
          return
      }

    case cmdDebugTimeout:
      fmt.Printf("%d: timeout - sleeping for 5 s...\n",
          incoming[0]);
      time.Sleep(5 * time.Second);
      response[1] = rspNone
      response[2] = 5
      conn.WriteTo(response[:3], addr)
      return

    default:
      fmt.Printf("%d: (invalid command: %d)\n",
          incoming[0], incoming[1])
      response[1] = rspErr
      response[2] = errNotImplemented
      conn.WriteTo(response[:3], addr)
      return
  }
}

func main() {
  pinState  = make(map[byte]bool)
  mcpMemory = make([]uint16, 8)
  maxMemory = make([]uint8, 0x6F)

  maxMemory[0x01] = 0xFF
  maxMemory[0x03] = 0x07
  maxMemory[0x06] = 0x1F
  for i := 0; i < 0xF; i++ {
    maxMemory[0x20 + i] = 0x20
    maxMemory[0x40 + i] = 0x20
  }

  conn, err := net.ListenUDP("udp", &net.UDPAddr{ Port: 5588 })
  if err != nil { panic(err) }

  fmt.Println("Listening on :5588")

  for {
    if err != nil { panic(err) }

    incoming := make([]byte, 258)
    _, addr, err := conn.ReadFrom(incoming)
    if err != nil { panic(err) }

    go handleMessage(conn, incoming, addr)
  }
}

/* vim: set ts=2 sts=2 et sw=2 ai: */
