#include <lua.h>

#include <math.h>
#include <time.h>

#define SEC_TO_NSEC 1e9

static int aux_sleep(lua_State *L) {
  struct timespec sleep_for = { 0, 0 };

  lua_Number sec_frac = lua_tonumber(L, 1);
  sleep_for.tv_sec = (int) floor(sec_frac);
  sleep_for.tv_nsec = (int) floor( fmodl(sec_frac, 1) * SEC_TO_NSEC );

  /* By the way, it's totally valid to discard the return value, because the
     interrupt handler isn't even in our control! Lua will handle the interrupt
     long before we do, so our code will either see this return 0 or
     never return. */
  nanosleep(&sleep_for, NULL);

  return 0;
}

int luaopen_aux(lua_State *L) {
  lua_createtable(L, 0, 1);

  lua_pushcfunction(L, aux_sleep);
  lua_setfield(L, -2, "sleep");

  return 1;
}
