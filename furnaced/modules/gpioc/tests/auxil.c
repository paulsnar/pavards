#include <lua.h>

#include <time.h>
#include <math.h>

#include <stdlib.h>
#include <sys/random.h>

#define SEC_TO_NSEC 1e9
#define neuter(ident) do { free(ident); ident = NULL; } while (0)

static int auxil_sleep(lua_State *L) {
  struct timespec sleep_for = {
    .tv_sec = 0,
    .tv_nsec = 0,
  };

  lua_Number sec_frac = 0;
  if ( ! lua_isnil(L, 1)) {
    sec_frac = lua_tonumber(L, 1);
    sleep_for.tv_sec = (int) floor(sec_frac);
    sleep_for.tv_nsec = (int) floor(fmodl(sec_frac, 1) * SEC_TO_NSEC);
  }

  if (nanosleep(&sleep_for, NULL) < 0) {
    lua_pushliteral(L, "sleep interrupted!");
    lua_error(L);
    return -1;
  }

  return 0;
}

static int auxil_randbytes(lua_State *L) {
  size_t bytecount = 1;
  if ( ! lua_isnil(L, 1)) {
    bytecount = lua_tointeger(L, 1);
  }

  unsigned char *bytes = malloc(bytecount);
  if (bytes == NULL) {
    lua_pushliteral(L, "out of memory");
    lua_error(L);
    return -1;
  }

  ssize_t got_bytes = getrandom(bytes, bytecount, 0);
  if (got_bytes != bytecount) {
    neuter(bytes);
    lua_pushliteral(L, "kernel provided too few bytes");
    lua_error(L);
    return -1;
  }

  lua_pushlstring(L, bytes, bytecount);
  neuter(bytes);
  return 1;
}

int luaopen_auxil(lua_State *L) {
  lua_createtable(L, 0, 2);

  lua_pushcfunction(L, auxil_sleep);
  lua_setfield(L, -2, "sleep");

  lua_pushcfunction(L, auxil_randbytes);
  lua_setfield(L, -2, "randbytes");

  return 1;
}

/* vim: set ts=2 sts=2 et sw=2: */
