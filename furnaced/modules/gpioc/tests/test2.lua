--[[ Test timeout behaviour

  NOTE: this exploits a debug method implemented only in gpiod-mock.
        This test will (or at least should) fail with production gpiod.

  To prevent deadlocks and general UDP unreliability, the gpioc socket
  has a builtin timeout of 1 second. The 'T' method of gpiod-mock will
  send a reply after 5 seconds, which should leave this side of the
  connection hanging on, so it should fail after one second with a
  "timed out" error message.

--]]

local gpioc, auxil =
    require('gpioc'),
    require('auxil')

local ok, err = pcall(gpioc.request, { method = 'T' })
if ok then
    error('test failed!')
end
-- note: gpiod-mock is parallelized, so this shouldn't really require sleeping
--[[ auxil.sleep(4.0) --]]
gpioc.request({ method = 'V' })
