/*
 * Interface to gpiod
 *
 * (c) 2017 paulsnar <paulsnar@paulsnar.lv>
 *
 * License notice should go here.
 */

#include <lua.h>

#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/random.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef int fd_t;

#define LUAERR(err) do { lua_pushliteral(L, err); lua_error(L); return -1; } while (0)
#define neuter(val) do { free(val); val = NULL; } while (0)

/* sequence[1] + resptype[1] + length[1] + data[0-255] */
#define MAX_REPLY_LEN 258

#define RSP_CONTENT_NONE '~'
#define RSP_CONTENT_SHRT '|'
#define RSP_CONTENT_LONG '}'
#define RSP_ERROR        '!'

#define GPIOERR_NOTIMPLEMENTED  0x28
#define GPIOERR_GPIO            0x29
#define GPIOERR_BADREQUEST      0x30
#define GPIOERR_REFUSED         0x31

static const char
  *gpio_error_not_implemented = "not implemented",
  *gpio_error_gpio = "GPIO fault",
  *gpio_error_bad_request = "bad request",
  *gpio_error_refused = "request refused";

static const char* gpioc_u_rsp_strerror(unsigned char gpioerr) {
  switch (gpioerr) {
    case GPIOERR_NOTIMPLEMENTED:  return gpio_error_not_implemented;
    case GPIOERR_GPIO:            return gpio_error_gpio;
    case GPIOERR_BADREQUEST:      return gpio_error_bad_request;
    case GPIOERR_REFUSED:         return gpio_error_refused;
    default:                      return NULL;
  }
}

static void *gpioc_regkey_socket;
#define SOCKET_TIMEOUT_SEC 1
#define SOCKET_TIMEOUT_USEC 0

static inline fd_t gpioc_u_init_socket() {
  fd_t sck = socket(AF_INET, SOCK_DGRAM, 0);
  if (sck == -1) return sck;

  struct sockaddr_in addr = {
    .sin_family = AF_INET,
    .sin_port = htons(5588),
  };
  memset(&addr.sin_addr, 0, sizeof(addr.sin_addr));
  inet_aton("127.0.0.1", &addr.sin_addr);

  if (connect(sck,
        (const struct sockaddr*) &addr,
        sizeof(struct sockaddr_in)) != 0) {
    close(sck);
    return -1;
  }

  struct timeval timeout = {
    .tv_sec = SOCKET_TIMEOUT_SEC,
    .tv_usec = SOCKET_TIMEOUT_USEC,
  };
  if (setsockopt(sck, SOL_SOCKET,
        SO_RCVTIMEO, &timeout, sizeof(struct timeval)) != 0) {
    close(sck);
    return -1;
  }
  if (setsockopt(sck, SOL_SOCKET,
        SO_SNDTIMEO, &timeout, sizeof(struct timeval)) != 0) {
    close(sck);
    return -1;
  }

  return sck;
}

static inline fd_t gpioc_u_obtain_socket(lua_State *L) {
  fd_t sck;

  lua_pushlightuserdata(L, gpioc_regkey_socket);
  lua_gettable(L, LUA_REGISTRYINDEX);

  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);
    sck = gpioc_u_init_socket();
    if (sck == -1) return sck;

    lua_pushlightuserdata(L, gpioc_regkey_socket);
    lua_pushinteger(L, sck);
    lua_settable(L, LUA_REGISTRYINDEX);

    return sck;
  } else {
    sck = lua_tointeger(L, -1);
    lua_pop(L, 1);
    
    return sck;
  }
}

static int gpioc_handle_gc(lua_State *L) {
  fd_t sck;

  lua_pushlightuserdata(L, gpioc_regkey_socket);
  lua_gettable(L, LUA_REGISTRYINDEX);

  if ( ! lua_isnil(L, -1)) {
    sck = lua_tointeger(L, -1);
    close(sck);
  }
  
  lua_pop(L, 1);
  return 0;
}

static inline int gpioc_u_has_field(
    lua_State *L, int idx, const char *field) {

  lua_getfield(L, idx, field);
  return ! lua_isnil(L, -1);
}

static inline void gpioc_u_copy_data(
    lua_State *L, int val_idx,
    unsigned char *target_buf, size_t length) {

  int buftype = lua_type(L, val_idx);

  unsigned char item;
  unsigned char *databuf;
  size_t real_len = length;

  if (buftype == LUA_TSTRING) {
    databuf = (unsigned char*) lua_tolstring(L, val_idx, &real_len);
    /* NB: real_len reflects the string length how Lua sees it, e.g., it
       doesn't actually include the \0 at the end. If databuf now points to
       "abc\0", real_len is 3. Just for reference. */
    if (real_len > length) {
      /* this is a really weird problem to have */
      lua_pushliteral(L, "data buffer length mutated?");
      lua_error(L);
      return;
    }
    memcpy(target_buf, databuf, real_len);
  } else if (buftype == LUA_TTABLE) {
    for (size_t i = 0; i < real_len; i++) {
      lua_geti(L, val_idx, i + 1);
      item = (unsigned char) lua_tointeger(L, -1);
      lua_pop(L, 1);

      target_buf[i] = item;
    }
  }
}

static void gpioc_u_flush_socket_queue(fd_t sck) {
  int sck_flags = 0;
  int received;

  sck_flags = fcntl(sck, F_GETFL);
  if (sck_flags < 0) goto fail;

  if ( ! (sck_flags & O_NONBLOCK)) {
    sck_flags |= O_NONBLOCK;
    if (fcntl(sck, F_SETFL, sck_flags) < 0) goto fail;
  }

  do {
    received = recv(sck, NULL, 0, 0);
  } while (received >= 0);

fail:
  if (sck_flags > 0 && sck_flags & O_NONBLOCK) {
    sck_flags ^= O_NONBLOCK;
    fcntl(sck, F_SETFL, sck_flags);
  }
  return;
}

static int gpioc_request(lua_State *L) {
  if (lua_type(L, 1) != LUA_TTABLE) LUAERR("first argument must be a table");

  fd_t sck = gpioc_u_obtain_socket(L);
  if (sck == -1) {
    lua_pushfstring(L, "couldn't open socket: (%d) %s",
      errno, strerror(errno));
    lua_error(L);
    return -1;
  }

  /* NOTE: If we timed out before, there's a fair chance that the message
     has arrived later and is still sitting in the socket queue. Therefore
     we first attempt to flush out the queue by reading in a non-blocking
     fashion. */
  gpioc_u_flush_socket_queue(sck);

  unsigned char sequence = 0;
  if (gpioc_u_has_field(L, 1, "seq")) {
    sequence = (unsigned char) lua_tointeger(L, -1);
  } else {
    getrandom(&sequence, sizeof(unsigned char), 0);
  }
  lua_pop(L, 1);

  unsigned char method = 0;
  if ( ! gpioc_u_has_field(L, 1, "method")) {
    lua_pop(L, 1);
    LUAERR("no method specified");
  }
  if (lua_type(L, -1) == LUA_TSTRING) {
    /* hack to get first char of string (but it works) */
    method = *lua_tostring(L, -1);
  } else {
    method = (unsigned char) lua_tointeger(L, -1);
  }
  lua_pop(L, 1);

  size_t data_len = 0;
  if (gpioc_u_has_field(L, 1, "data")) {
    lua_len(L, -1);
    data_len = lua_tointeger(L, -1);
    lua_pop(L, 1); /* #arg.data */
  }
  lua_pop(L, 1); /* arg.data */

  if (data_len > 255) LUAERR("data buffer too large");

  unsigned char *sendbuf = NULL;
  size_t sendbuf_len;
  if (data_len > 0) {
    sendbuf_len = 2 + data_len;
  } else {
    sendbuf_len = 2;
  }
  sendbuf = malloc(sendbuf_len);
  if (sendbuf == NULL) LUAERR("out of memory");
  memset(sendbuf, 0, sendbuf_len);

  sendbuf[0] = sequence;
  sendbuf[1] = method;
  if (data_len > 0) {
    lua_getfield(L, 1, "data");
    gpioc_u_copy_data(L, lua_gettop(L), sendbuf + 2, data_len);
  }

  ssize_t sent = send(sck, sendbuf, sendbuf_len, 0);
  neuter(sendbuf);

  if (sent == -1 || sent != sendbuf_len) {
    lua_pushfstring(L, "send failed: (%d) %s",
      errno, strerror(errno));
    lua_error(L);
    return -1;      
  }

  unsigned char *reply = NULL;
  size_t reply_len = MAX_REPLY_LEN;

  reply = malloc(reply_len);
  if (reply == NULL) LUAERR("out of memory");
  memset(reply, 0, reply_len);

  reply_len = recv(sck, reply, reply_len, 0);
  if (reply_len == -1) {
    neuter(reply);
    if (errno == EAGAIN) LUAERR("timed out");
    lua_pushfstring(L, "receive failed: (%d) %s",
      errno, strerror(errno));
    lua_error(L);
    return -1;
  }

  if (reply[1] == RSP_ERROR) {
    lua_pushfstring(L, "server error: (%d) %s",
      reply[2], gpioc_u_rsp_strerror(reply[2]));
    neuter(reply);
    lua_error(L);
    return -1;
  }

  /* Should this be considered an error condition?
     For what it's worth, gpiod is single-threaded, and because both sides of
     the connection are blocking, a sequence mismatch is unlikely, and it's
     much more possible that something else would go horribly wrong (like a
     deadlock). This can become a problem when using gpiod over a network
     boundary (which you shouldn't ever do, by the way), but that's exactly
     why I would consider that fatal. Please don't use gpiod over the network,
     it's not engineered to really be safe (like Redis). If you absolutely
     must, I guess call #request in protected mode or something. */
  if (reply[0] != sequence) LUAERR("out-of-order response");

  lua_createtable(L,
    reply[1] == RSP_CONTENT_LONG ?
      (reply_len - 2) / 2 : (reply_len - 2),
    1
  );
  int resp_table_pos = lua_gettop(L);

  /* hack to push the response char as a string */
  lua_pushlstring(L, reply + 1, 1);
  lua_setfield(L, -2, "response");

  lua_pushinteger(L, reply[0]);
  lua_setfield(L, -2, "sequence");

  unsigned char items = reply[2];

  if (reply[1] == RSP_CONTENT_LONG) {
    size_t offset = 3;
    for (size_t i = 0; i < items; i += 1) {
      lua_pushinteger(L,
        (reply[offset] << 8) | (reply[offset + 1]));
      lua_seti(L, -2, i + 1); /* fucking 1-based indexing */
      offset += 2;
    }
  } else {
    for (size_t i = 0; i < items; i += 1) {
      lua_pushinteger(L, reply[i + 3]);
      lua_seti(L, -2, i + 1);
    }
  }

  lua_pushlstring(L, reply, reply_len);
  lua_setfield(L, -2, "raw_data");

  neuter(reply);

  return 1;
}

int luaopen_gpioc(lua_State *L) {
  gpioc_regkey_socket = &gpioc_regkey_socket;

  lua_createtable(L, 0, 1);
  
  lua_pushcfunction(L, gpioc_request);
  lua_setfield(L, -2, "request");

  lua_createtable(L, 0, 1);
  lua_pushcfunction(L, gpioc_handle_gc);
  lua_setfield(L, -2, "__gc");
  lua_setmetatable(L, -2);

  return 1;
}
/* vim: set ts=2 sts=2 et sw=2: */
