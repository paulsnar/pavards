/*
 * Persistenece layer
 *
 * © 2017 paulsnar <paulsnar@paulsnar.lv>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License in the LICENSE.txt file in a nearby
 * directory, in /usr/share/licenses/common/Apache/license.txt or by visiting
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <gdbm.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

#define DB_DEFAULT_NAME "/var/lib/furnaced-persist.db"

#define REGISTRY_FIELD_DB "persist.gdbm"

static int persist__close(lua_State *L) {
  GDBM_FILE db;

  lua_getfield(L, LUA_REGISTRYINDEX, REGISTRY_FIELD_DB);
  if ( ! lua_isnil(L, -1)) {
    db = lua_touserdata(L, -1);

    gdbm_close(db);

    lua_pop(L, 1);

    lua_pushnil(L);
    lua_setfield(L, LUA_REGISTRYINDEX, REGISTRY_FIELD_DB);
  }

  return 0;
}

static int persist__open(lua_State *L) {
  GDBM_FILE db;

  if (lua_isnil(L, 1)) {
    lua_pushliteral(L, DB_DEFAULT_NAME);
    lua_replace(L, 1);
  } else if ( ! lua_isstring(L, 1)) {
    lua_pushliteral(L, "bad DB path argument type - expected string");
    lua_error(L);
    return -1;
  }

  lua_getfield(L, LUA_REGISTRYINDEX, REGISTRY_FIELD_DB);
  if ( ! lua_isnil(L, -1)) {
    lua_pop(L, 1);
    lua_pushcfunction(L, persist__close);
    lua_call(L, 0, 0);
    db = lua_touserdata(L, -1);
    lua_pop(L, 1);
    gdbm_close(db);
  }

  const char *path = lua_tostring(L, 1);
  db = gdbm_open(path, 0, GDBM_WRCREAT | GDBM_SYNC,
    S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, NULL);
  if (db == NULL) {
    lua_pushfstring(L, "open db failed: %s", gdbm_strerror(gdbm_errno));
    lua_error(L);
    return -1;
  }

  lua_pushlightuserdata(L, db);
  lua_setfield(L, LUA_REGISTRYINDEX, REGISTRY_FIELD_DB);

  lua_pushlightuserdata(L, db);
  return 1;
}

static int persist__obtain_db(lua_State *L) {
  GDBM_FILE db;

  lua_getfield(L, LUA_REGISTRYINDEX, REGISTRY_FIELD_DB);
  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);

    lua_pushcfunction(L, persist__open);
    lua_pushnil(L);
    lua_call(L, 1, 1);
  }

  db = lua_touserdata(L, -1);
  lua_pop(L, 1);

  lua_pushlightuserdata(L, db);
  return 1;
}

static int persist__count(GDBM_FILE db, lua_State *L) {
  gdbm_count_t pcount = 0;

  if (gdbm_count(db, &pcount) == -1) {
    lua_pushfstring(L, "count failed: %s", gdbm_strerror(gdbm_errno));
    lua_error(L);
    return -1;
  }

  lua_pushinteger(L, pcount);
  return 1;
}

static int persist_index(lua_State *L) {
  if ( ! lua_isstring(L, 2)) {
    lua_pushliteral(L, "only string keys are supported");
    lua_error(L);
    return -1;
  }

  lua_pushcfunction(L, persist__obtain_db);
  lua_call(L, 0, 1);
  GDBM_FILE db = lua_touserdata(L, -1);
  lua_pop(L, 1);

  size_t key_len = 0;
  const char *key = lua_tolstring(L, 2, &key_len);

  if (strncmp("__count", key, key_len) == 0) {
    return persist__count(db, L);
  }

  datum key_d = { (char*) key, key_len };

  datum val_d = gdbm_fetch(db, key_d);

  if (val_d.dptr == NULL) {
    if (gdbm_errno == GDBM_ITEM_NOT_FOUND) {
      lua_pushnil(L);
      return 1;
    } else {
      lua_pushfstring(L, "get failed: %s", gdbm_strerror(gdbm_errno));
      lua_error(L);
      return -1;
    }
  }

  lua_pushlstring(L, val_d.dptr, val_d.dsize);

  free(val_d.dptr);

  return 1;
}

static int persist_newindex(lua_State *L) {
  if ( ! lua_isstring(L, 2)) {
    lua_pushliteral(L, "only string keys are supported");
    lua_error(L);
    return -1;
  }

  if ( ! (lua_isstring(L, 3) || lua_isnil(L, 3))) {
    lua_pushliteral(L, "only string values are supported");
    lua_error(L);
    return -1;
  }

  lua_pushcfunction(L, persist__obtain_db);
  lua_call(L, 0, 1);
  GDBM_FILE db = lua_touserdata(L, -1);
  lua_pop(L, 1);

  size_t key_len = 0;
  const char *key = lua_tolstring(L, 2, &key_len);
  datum key_d = { (char*) key, key_len };

  if (lua_isnil(L, 3)) {
    if (gdbm_delete(db, key_d) != 0) {
      if (gdbm_errno == GDBM_ITEM_NOT_FOUND) {
        return 0;
      } else {
        lua_pushfstring(L, "delete failed: %s", gdbm_strerror(gdbm_errno));
        lua_error(L);
        return -1;
      }
    }
    return 0;
  }

  size_t val_len = 0;
  const char *val = lua_tolstring(L, 3, &val_len);
  datum val_d = { (char*) val, val_len };

  if (gdbm_store(db, key_d, val_d, GDBM_REPLACE) != 0) {
    lua_pushfstring(L, "set failed: %s", gdbm_strerror(gdbm_errno));
    lua_error(L);
    return -1;
  }

  return 0;
}

static int persist__iter(lua_State *L) {
  // iter(table_or_rather_nil, next_key_or_nil)
  if ( ! (lua_isstring(L, 2) || lua_isnil(L, 2))) {
    lua_pushliteral(L, "invalid key type given");
    lua_error(L);
    return -1;
  }

  lua_pushcfunction(L, persist__obtain_db);
  lua_call(L, 0, 1);
  GDBM_FILE db = lua_touserdata(L, -1);
  lua_pop(L, 1);

  datum key_d;

  if (lua_isnil(L, 2)) {
    key_d = gdbm_firstkey(db);

    goto skip_prev_key;
  }

  size_t prev_key_len = 0;
  const char *prev_key = lua_tolstring(L, 2, &prev_key_len);

  datum prev_key_d = { (char*) prev_key, prev_key_len };
  key_d = gdbm_nextkey(db, prev_key_d);

skip_prev_key:
  if (key_d.dptr == NULL) {
    if (gdbm_errno == GDBM_ITEM_NOT_FOUND) {
      lua_pushnil(L);
      return 1;
    } else {
      lua_pushfstring(L, "iteration failed: %s", gdbm_strerror(gdbm_errno));
      lua_error(L);
      return -1;
    }
  }

  lua_pushlstring(L, key_d.dptr, key_d.dsize);
  lua_pushcfunction(L, persist_index);
  lua_pushnil(L);
  lua_pushvalue(L, -3);
  lua_call(L, 2, 1);

  free(key_d.dptr);

  return 2;
}

static int persist_pairs(lua_State *L) {
  lua_pushcfunction(L, persist__iter);
  lua_pushnil(L);
  lua_pushnil(L);
  return 3;
}

static int persist_gc(lua_State *L) {
  lua_pushcfunction(L, persist__close);
  lua_call(L, 0, 0);

  return 0;
}

static const luaL_Reg persist_magic[] = {
  { "__open", persist__open },
  { "__close", persist__close },
  { NULL, NULL },
};

static const luaL_Reg persist_meta[] = {
  { "__index", persist_index },
  { "__newindex", persist_newindex },
  { "__pairs", persist_pairs },
  { "__gc", persist_gc },
  { NULL, NULL },
};

int luaopen_c_persist(lua_State *L) {
  lua_newtable(L); // always empty magic storage table
  luaL_setfuncs(L, persist_magic, 0); // well, almost

  lua_newtable(L); // metatable
  luaL_setfuncs(L, persist_meta, 0);

  lua_setmetatable(L, -2);
  return 1;
}
