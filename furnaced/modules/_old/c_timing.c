/*
 * Timing adapter
 *
 * © 2017 paulsnar <paulsnar@paulsnar.lv>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License in the LICENSE.txt file in a nearby
 * directory, in /usr/share/licenses/common/Apache/license.txt or by visiting
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <string.h>
#include <time.h>

typedef enum { M, S, MS } timing_tunit;

static int timing_suspend_for(lua_State *L) {
  // int n = lua_gettop(L);
  // assert(n == 1 || n == 2);

  timing_tunit unit;

  struct timespec sleep_for;
  memset(&sleep_for, 0, sizeof(struct timespec));

  sleep_for.tv_nsec = lua_tonumber(L, 1);
  sleep_for.tv_sec = lua_tonumber(L, 2);
  nanosleep(&sleep_for, NULL);

  return 0;
}

static const luaL_Reg c_timing_lib[] = {
  { "suspend_for", timing_suspend_for },
  { NULL, NULL },
};

int luaopen_c_timing(lua_State *L) {
  lua_newtable(L);
  luaL_setfuncs(L, c_timing_lib, 0);

  return 1;
}
