-- vim: set ts=2 sts=2 et sw=2:

local test = { }

local assertions = 0

function test.reset_counter() assertions = 0 end
function test.get_counter() return assertions end

local STDOUT = io.output()

local function deep_equal(a, b)
  if type(a) ~= type(b) then return false end
  if type(a) ~= 'table' then return a == b end

  local seen_keys = { }

  for key, _ in pairs(a) do
    seen_keys[key] = true
    if not deep_equal(a[key], b[key]) then return false end
  end

  for key, _ in pairs(b) do
    if not seen_keys[key] then return false end
  end
  
  return true
end

test.fail = error

function test.expect_equal(thing, target)
  if not deep_equal(thing, target) then
    error('equality expectation failed!', 2)
  else
    STDOUT:write('.')
    assertions = assertions + 1
  end
end

function test.expect_error(func, ...)
  success = pcall(func, ...)
  if success ~= false then
    error('failure expectation failed!', 2)
  else
    STDOUT:write('.')
    assertions = assertions + 1
  end
end

return test
