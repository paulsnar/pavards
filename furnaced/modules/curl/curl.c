/*
 * Lua interface to cURL
 *
 * (c) 2017 paulsnar <paulsnar@paulsnar.lv>
 *
 * License notice should go here.
 */

#include <lua.h>

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>

/* #define LUACURL_ENABLE_DEBUG 1 */

#define neuter(n) do { free(n); n = NULL; } while (0)

static void *luacurl_regkey_curl_handle;

static inline CURL* luacurl_i_init_curl_handle() {
  CURL *ch = curl_easy_init();
  if (ch) {
    curl_easy_setopt(ch, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
    curl_easy_setopt(ch, CURLOPT_DEFAULT_PROTOCOL, "https");
#ifdef LUACURL_ENABLE_DEBUG
    curl_easy_setopt(ch, CURLOPT_VERBOSE, 1L);
#endif /* LUACURL_ENABLE_DEBUG */
  }
  return ch;
}

static inline CURL* luacurl_i_obtain_curl_handle(lua_State *L) {
  CURL *ch;

  lua_pushlightuserdata(L, luacurl_regkey_curl_handle);
  lua_gettable(L, LUA_REGISTRYINDEX);

  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);
    ch = luacurl_i_init_curl_handle();
    if ( ! ch) return ch;

    lua_pushlightuserdata(L, luacurl_regkey_curl_handle);
    lua_pushlightuserdata(L, ch);
    lua_settable(L, LUA_REGISTRYINDEX);

    return ch;
  } else {
    ch = lua_touserdata(L, -1);
    lua_pop(L, 1);

    return ch;
  }
}

static int luacurl_handle_gc(lua_State *L) {
  CURL *ch;

  lua_pushlightuserdata(L, luacurl_regkey_curl_handle);
  lua_gettable(L, LUA_REGISTRYINDEX);

  if ( ! lua_isnil(L, -1)) {
    ch = lua_touserdata(L, -1);

    curl_easy_cleanup(ch);
  }

  lua_pop(L, 1);
  
  return 0;
}

#define LUACURL_DEFAULT_BUFFER_SIZE 512 * sizeof(char)
#define LUACURL_MAX_HEADER_LENGTH 1024

struct luacurl_headerhandler_item {
  char *name_buf;
  char *data_buf;

  size_t name_len;
  size_t data_len;

  struct luacurl_headerhandler_item *next;
};

struct luacurl_headerhandler_state {
  size_t n;
  struct luacurl_headerhandler_item *first;
  struct luacurl_headerhandler_item *last;
};

static size_t luacurl_request_header_handler(
    char *buffer, size_t size, size_t nmemb, void *userdata) {

  struct luacurl_headerhandler_state *s = userdata;
  size_t real_size = size * nmemb;

  char *separator = memchr(buffer, ':', real_size);

  if (separator == NULL) {
    // malformed or faux header (status line?)
    return real_size;
  }

  struct luacurl_headerhandler_item *header =
      malloc(sizeof(struct luacurl_headerhandler_item));
  if (header == NULL) return 0;

  memset(header, 0, sizeof(struct luacurl_headerhandler_item));

  header->name_len = separator - buffer;
  header->data_len = real_size - header->name_len - 1;
  char *c;
  for (size_t i = header->data_len; i >= 0; i--) {
    c = separator + 1 + i;
    if (*c == '\r' || *c == '\n' || *c == 0) {
      // strip trailing whitespace
      header->data_len -= 1;
    } else break;
  }

  header->name_buf = malloc(header->name_len);
  header->data_buf = malloc(header->data_len);
  if (header->name_buf == NULL || header->data_buf == NULL) {
    if (header->name_buf != NULL) free(header->name_buf);
    if (header->data_buf != NULL) free(header->data_buf);
    free(header);
    return 0;
  }

  size_t skip_whitespace;
  for (skip_whitespace = 0; skip_whitespace < header->data_len;
      skip_whitespace++) {
    c = separator + 1 + skip_whitespace;
    // I really hope your leading whitespace is not important!
    if (*c != ' ') break;
  }

  memcpy(header->name_buf, buffer, header->name_len);
  memcpy(header->data_buf,
      separator + 1 /* colon */ + skip_whitespace,
      header->data_len);

  if (s->last == NULL) {
    s->first = s->last = header;
  } else {
    s->last->next = header;
    s->last = header;
  }

  return real_size;
}

static inline void luacurl_u_transform_headers(
    struct luacurl_headerhandler_state* s, lua_State *L, int headertable_pos) {

  struct luacurl_headerhandler_item
      *header_prev = NULL,
      *header_current = s->first;
  size_t i;
  while (header_current != NULL) {
    if (header_prev != NULL) {
      neuter(header_prev->name_buf);
      neuter(header_prev->data_buf);
      neuter(header_prev);
    }

    for (i = 0; i < header_current->name_len; i++) {
      header_current->name_buf[i] = tolower(header_current->name_buf[i]);
    }
    lua_pushlstring(L, header_current->name_buf, header_current->name_len);
    lua_pushlstring(L, header_current->data_buf, header_current->data_len);
    lua_settable(L, -3);

    header_prev = header_current;
    header_current = header_current->next;
  }
}

static inline void luacurl_u_cleanup_headers(
    struct luacurl_headerhandler_state* s) {

  struct luacurl_headerhandler_item
      *header_prev = NULL,
      *header_current = s->first;

  while (header_current != NULL) {
    if (header_prev != NULL) {
      neuter(header_prev->name_buf);
      neuter(header_prev->data_buf);
      neuter(header_prev);
    }

    header_prev = header_current;
    header_current = header_current->next;
  }
}

struct luacurl_iohandler_state {
  char *buffer;
  size_t bufsize;
  size_t position;
  size_t total;
};

#define LUACURL_SIZE_INCREASE(s, req) \
    s->bufsize + req + LUACURL_DEFAULT_BUFFER_SIZE

static size_t luacurl_request_write_handler(
    char *data, size_t size, size_t nmemb, void *userdata) {

  struct luacurl_iohandler_state *s = userdata;
  size_t real_size = size * nmemb;

  char *newbuf;
  if (size * nmemb > (s->bufsize - s->total)) {
    newbuf = realloc(s->buffer, LUACURL_SIZE_INCREASE(s, real_size));
    if (newbuf == NULL) {
      neuter(s->buffer);
      return 0;
    }
    s->buffer = newbuf;
    s->bufsize = LUACURL_SIZE_INCREASE(s, real_size);
  }

  memcpy(s->buffer + s->position, data, real_size);
  s->position += real_size;
  s->total = s->position;

  return real_size;
}

static size_t luacurl_request_read_handler(
    char *buffer, size_t size, size_t nmemb, void *userdata) {

  struct luacurl_iohandler_state *s = userdata;
  size_t real_size = size * nmemb;

  if (s->position == s->total) {
    return 0;
  } else {
    memcpy(buffer, s->buffer + s->position, real_size);
    s->position += real_size;
  }

  return real_size;
}

static int luacurl_handle_response_headers_index(lua_State *L) {
  const char *key;
  size_t key_len;

  if ( ! lua_isstring(L, 2)) {
    return 0;
  } else {
    key = lua_tolstring(L, 2, &key_len);
  }

  char *key_lower = malloc(key_len);
  if (key_lower == NULL) {
    lua_pushliteral(L, "out of memory");
    lua_error(L);
    return -1;
  }

  for (size_t i = 0; i< key_len; i++) {
    key_lower[i] = tolower(key[i]);
  }
  lua_pop(L, 1);

  lua_pushlstring(L, key_lower, key_len);
  lua_rawget(L, 1);

  neuter(key_lower);
  return 1;
}

#define LCERR(msg) do { lua_pushliteral(L, msg); lua_error(L); return -1; } while (0)

static int luacurl_request(lua_State *L) {
  if (lua_type(L, 1) != LUA_TTABLE) LCERR("first argument must be a table");

  CURL *ch = luacurl_i_obtain_curl_handle(L);
  if (ch == NULL) LCERR("out of memory");

  lua_getfield(L, 1, "method");
  const char *method = lua_tostring(L, -1);
  if (method == NULL) LCERR("invalid method specified");

  if (strncmp(method, "GET", 4) == 0) {
    curl_easy_setopt(ch, CURLOPT_HTTPGET, 1L);
  } else if (strncmp(method, "POST", 5) == 0) {
    curl_easy_setopt(ch, CURLOPT_POST, 1L);
  } else {
    curl_easy_setopt(ch, CURLOPT_CUSTOMREQUEST, method);
  }
  method = NULL;
  lua_pop(L, 1);

  lua_getfield(L, 1, "url");
  const char *url = lua_tostring(L, -1);
  if (url == NULL) LCERR("invalid url specified");
  curl_easy_setopt(ch, CURLOPT_URL, url);
  url = NULL;
  lua_pop(L, 1);

  struct luacurl_iohandler_state response;
  memset(&response, 0, sizeof(struct luacurl_iohandler_state));
  response.buffer = malloc(LUACURL_DEFAULT_BUFFER_SIZE);
  if (response.buffer == NULL) LCERR("out of memory");
  response.bufsize = LUACURL_DEFAULT_BUFFER_SIZE;

  curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION, luacurl_request_write_handler);
  curl_easy_setopt(ch, CURLOPT_WRITEDATA, &response);

  struct luacurl_iohandler_state request;
  memset(&request, 0, sizeof(struct luacurl_iohandler_state));
  lua_getfield(L, 1, "content");
  if (lua_isnil(L, -1)) {
    request.total = 0;
  } else {
    request.buffer = (char*) lua_tolstring(L, -1, &request.total);
    request.position = 0;
  }
  /* NOTE: we pop content after performing the request so that Lua doesn't
     GC it prematurely! */
  curl_easy_setopt(ch, CURLOPT_READFUNCTION, luacurl_request_read_handler);
  curl_easy_setopt(ch, CURLOPT_READDATA, &request);

  struct curl_slist *headers = NULL;
  char *header = malloc(LUACURL_MAX_HEADER_LENGTH * sizeof(char));
  if (header == NULL) LCERR("out of memory");
  lua_getfield(L, 1, "headers");
  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);
    neuter(header);
  } else {
    lua_pushnil(L);
    while (lua_next(L, -2) != 0) {
      snprintf(header, LUACURL_MAX_HEADER_LENGTH,
          "%s: %s", lua_tostring(L, -2), lua_tostring(L, -1));
      headers = curl_slist_append(headers, header);
      lua_pop(L, 1);
    }
    lua_pop(L, 1); /* the header table */

    /* I don't think cURL adds that for us? */
    if (request.total > 0) {
      snprintf(header, LUACURL_MAX_HEADER_LENGTH,
          "Content-Length: %zu", request.total);
      headers = curl_slist_append(headers, header);
    }
    neuter(header);

    curl_easy_setopt(ch, CURLOPT_HTTPHEADER, headers);
  }

  struct luacurl_headerhandler_state in_headers;
  memset(&in_headers, 0, sizeof(struct luacurl_headerhandler_state));

  curl_easy_setopt(ch, CURLOPT_HEADERFUNCTION, luacurl_request_header_handler);
  curl_easy_setopt(ch, CURLOPT_HEADERDATA, &in_headers);

  CURLcode curl_result = curl_easy_perform(ch);
  curl_slist_free_all(headers);
  headers = NULL;

  lua_pop(L, 1); /* request body from before */
  request.buffer = NULL;

  if (curl_result != CURLE_OK) {
    /* whoops! */
    if (in_headers.first) {
      luacurl_u_cleanup_headers(&in_headers);
    }
    neuter(response.buffer);

    lua_pushfstring(L, "cURL error: %s",
      curl_easy_strerror(curl_result));
    lua_error(L);
    return -1;
  }

  /* response table */
  lua_newtable(L);

  long status;
  curl_easy_getinfo(ch, CURLINFO_RESPONSE_CODE, &status);
  lua_pushinteger(L, status);
  lua_setfield(L, -2, "status");

  /* header table */
  lua_newtable(L);
  int header_table_pos = lua_gettop(L);
  luacurl_u_transform_headers(&in_headers, L, header_table_pos);

  lua_newtable(L);
  lua_pushcfunction(L, luacurl_handle_response_headers_index);
  lua_setfield(L, -2, "__index");
  lua_setmetatable(L, -2);
  lua_setfield(L, -2, "headers");

  if (response.total > 0) {
    lua_pushlstring(L, response.buffer, response.total);
    lua_setfield(L, -2, "content");
  }
  neuter(response.buffer);

  return 1;
}

static int luacurl_enable_debug(lua_State *L) {
  CURL *ch = luacurl_i_obtain_curl_handle(L);
  if (ch == NULL) LCERR("out of memory");

  curl_easy_setopt(ch, CURLOPT_VERBOSE, 1L);

  lua_pushboolean(L, 1);
  return 1;
}

int luaopen_curl(lua_State *L) {
  luacurl_regkey_curl_handle = &luacurl_regkey_curl_handle;

  if (curl_global_init(CURL_GLOBAL_SSL) != CURLE_OK) {
    lua_pushliteral(L, "could not globally init cURL");
    lua_error(L);
    return -1;
  }

  lua_createtable(L, 0, 3);

  lua_pushcfunction(L, luacurl_request);
  lua_setfield(L, -2, "request");

  lua_pushcfunction(L, luacurl_enable_debug);
  lua_setfield(L, -2, "enable_debug");

  lua_createtable(L, 0, 1);
  lua_pushcfunction(L, luacurl_handle_gc);
  lua_setfield(L, -2, "__gc");
  lua_setmetatable(L, -2);

  return 1;
}

/* vim: set ts=2 sts=2 et sw=2: */
