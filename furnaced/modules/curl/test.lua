local curl = require('curl')

if arg[1] == '--debug' then
    curl.enable_debug()
end

local resp = curl.request({
    method = 'GET',
    url = 'http://localhost:8080/test.json',
    headers = {
        ['User-Agent'] = 'lua+curl/0.0.2',
    },
})

print('status', resp.status)
print('content', resp.content)
print('headers.server', resp.headers.server)
