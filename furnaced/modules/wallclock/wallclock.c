#include <lua.h>

#include <time.h>

#include <errno.h>
#include <string.h>

#define NSEC_TO_SEC 1e9

#define SECOND        1
#define SECOND_FLOAT  1.0

#define MILLISECOND SECOND_FLOAT / 1000
#define MICROSECOND MILLISECOND / 1000
#define NANOSECOND  MICROSECOND / 1000

#define MINUTE      60 * SECOND
#define HOUR        60 * MINUTE

static int wallclock_now(lua_State *L) {
  struct timespec now;
  memset(&now, 0, sizeof(struct timespec));

  if (clock_gettime(CLOCK_MONOTONIC_RAW, &now) < 0) {
    lua_pushfstring(L, "failed to get time: (%d) %s\n",
        errno, strerror(errno));
    lua_error(L);
    return -1;
  }

  lua_Number ts = (lua_Number) now.tv_sec;
  ts += ((double) now.tv_nsec) / NSEC_TO_SEC;

  lua_pushnumber(L, ts);
  return 1;
}

int luaopen_wallclock(lua_State *L) {
  lua_createtable(L, 0, 1);

  lua_pushcfunction(L, wallclock_now);
  lua_setfield(L, -2, "now");

  /* inject constants */

  lua_pushnumber(L, NANOSECOND);
  lua_setfield(L, -2, "NANOSECOND");

  lua_pushnumber(L, MICROSECOND);
  lua_setfield(L, -2, "MICROSECOND");

  lua_pushnumber(L, MILLISECOND);
  lua_setfield(L, -2, "MILLISECOND");  

  lua_pushinteger(L, SECOND);
  lua_setfield(L, -2, "SECOND");

  lua_pushinteger(L, MINUTE);
  lua_setfield(L, -2, "MINUTE");

  lua_pushinteger(L, HOUR);
  lua_setfield(L, -2, "HOUR");

  return 1;
}

/* vim: set ts=2 sts=2 et sw=2: */
