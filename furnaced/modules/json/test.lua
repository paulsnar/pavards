-- vim: set ts=2 sts=2 et sw=2:

local json = require('json')
local test = dofile('../testutils.lua')

test.reset_counter()

local decode_tests = {
  { 'null', nil },
  { '42', 42 },
  { '42.5', 42.5 },
  { '"hello"', 'hello' },
  { '[]', {} },
  { '{}', {} },
  { '[1, "hello", 3, true]', { 1, 'hello', 3, true } },
  { '{"hello":"world"}', { hello = 'world' } },
  { '{ "complex": ["objects", 42], "are": "cool", "right?": true }',
    { complex = {'objects', 42}, are = 'cool', ['right?'] = true } },
}
local decode_failures = {
  'test',
  '42.',
  '[',
  '{',
  '"string',
  '{"hello":',
  '{"hello"}',
  '[[][]]',
}

for _, tuple in pairs(decode_tests) do
  local input, expected = table.unpack(tuple)
  test.expect_equal( json.decode(input), expected )
end
for _, failing_input in pairs(decode_tests) do
  test.expect_error(json.decode, failing_input)
end

local encode_tests = {
  { nil, 'null' },
  { 42, '42' },
  { 42.5, '42.5' },
  { 'hello', '"hello"' },
  { {}, '{}' }, -- not a sequence, therefore not an array
  { {1, 2, 3}, '[1,2,3]' },
  { { hello = 'world'}, '{"hello":"world"}' },
}

for _, tuple in pairs(encode_tests) do
  local input, expected = table.unpack(tuple)
  test.expect_equal( json.encode(input), expected )
end

io.write( string.format('\n  Tests passed (%d assertions)\n', test.get_counter()) )
