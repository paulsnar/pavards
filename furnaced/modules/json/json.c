/*
 * Lua JSON encoder/decoder (via yajl)
 *
 * (c) 2017 paulsnar <paulsnar@paulsnar.lv>
 *
 * License notice should go here.
 */

#include <lua.h>

#include <stdint.h>

#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>

static inline void json_parse_cb_attempt_table(lua_State *L) {
  lua_Integer next_i;
  if (lua_istable(L, -3)) {
    lua_settable(L, -3);
  } else if (lua_istable(L, -2)) {
    lua_len(L, -2);
    next_i = lua_tointeger(L, -1) + 1;
    lua_pop(L, 1);
    lua_seti(L, -2, next_i);
  }
}

#define CTX_LUA(var_a, var_b) lua_State *var_b = (lua_State*) var_a

static int json_parse_cb_null(void *ctx) {
  CTX_LUA(ctx, L);
  lua_pushnil(L);
  json_parse_cb_attempt_table(L);
  return 1;
}

static int json_parse_cb_boolean(void *ctx, int val) {
  CTX_LUA(ctx, L);
  lua_pushboolean(L, val);
  json_parse_cb_attempt_table(L);
  return 1;
}

static int json_parse_cb_integer(void *ctx, long long val) {
  CTX_LUA(ctx, L);
  lua_pushinteger(L, val);
  json_parse_cb_attempt_table(L);
  return 1;
}

static int json_parse_cb_double(void *ctx, double val) {
  CTX_LUA(ctx, L);
  lua_pushnumber(L, val);
  json_parse_cb_attempt_table(L);
  return 1;
}

static int json_parse_cb_string(void *ctx, const unsigned char *b, size_t len) {
  CTX_LUA(ctx, L);
  lua_pushlstring(L, (const char*) b, len);
  json_parse_cb_attempt_table(L);
  return 1;
}

static int json_parse_cb_table_start(void *ctx) {
  CTX_LUA(ctx, L);
  lua_newtable(L);
  return 1;
}

static int json_parse_cb_table_key(void *ctx, const unsigned char *b, size_t len) {
  CTX_LUA(ctx, L);
  lua_pushlstring(L, (const char*) b, len);
  return 1;
}

static int json_parse_cb_table_end(void *ctx) {
  CTX_LUA(ctx, L);
  json_parse_cb_attempt_table(L);
  return 1;
}

static const yajl_callbacks json_parse_callbacks = {
  json_parse_cb_null,
  json_parse_cb_boolean,
  json_parse_cb_integer,
  json_parse_cb_double,
  NULL, /* all numbers -- instead handle ints/floats separately */
  json_parse_cb_string,
  json_parse_cb_table_start,
  json_parse_cb_table_key,
  json_parse_cb_table_end,
  json_parse_cb_table_start,
  json_parse_cb_table_end,
};

static yajl_gen_status json_encode_helper(
    yajl_gen h, lua_State *L, int offset) {

  size_t len;
  char *str;
  
  switch (lua_type(L, offset)) {
    case LUA_TNONE:
    case LUA_TNIL:
      return yajl_gen_null(h);

    case LUA_TBOOLEAN:
      return yajl_gen_bool(h,
        lua_toboolean(L, offset));

    case LUA_TNUMBER:
      if (lua_isinteger(L, offset)) {
        return yajl_gen_integer(h, lua_tointeger(L, offset));
      } else {
        return yajl_gen_double(h, lua_tonumber(L, offset));
      }

    case LUA_TSTRING:
      str = (char*) lua_tolstring(L, offset, &len);
      return yajl_gen_string(h, (const unsigned char*) str, len);

    default: /* fallthrough */;
  }

  lua_len(L, offset);
  len = (int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  yajl_gen_status result;

  if (len != 0) {
    // if there's a sequence part, we assume a list structure
    if ((result = yajl_gen_array_open(h)) != yajl_gen_status_ok) return result;
    for (size_t i = 1; i <= len; i++) {
      lua_geti(L, offset, i);
      if ((result = json_encode_helper(h, L, lua_gettop(L)))
          != yajl_gen_status_ok) return result;

      lua_pop(L, 1);
    }
    if ((result = yajl_gen_array_close(h)) != yajl_gen_status_ok) return result;
  } else {
    if ((result = yajl_gen_map_open(h)) != yajl_gen_status_ok) return result;
    lua_pushnil(L);
    while (lua_next(L, offset) != 0) {
      int key_type = lua_type(L, offset + 1);
      if (key_type == LUA_TNUMBER) {
        // stringify the key, since JSON object keys are strings
        lua_pushvalue(L, offset + 1); // but operate on a copy to not modify
        (void) lua_tostring(L, offset + 3); // the original
        if ((result = json_encode_helper(h, L, offset + 2))
            != yajl_gen_status_ok) return result;
        lua_pop(L, 1);
      } else if (key_type == LUA_TSTRING) {
        if ((result = json_encode_helper(h, L, offset + 1))
            != yajl_gen_status_ok) return result;
      } else {
        lua_pushliteral(L, "refusing to encode non-stringy keys");
        lua_error(L);
        return -1;
      }

      if ((result = json_encode_helper(h, L, offset + 2))
          != yajl_gen_status_ok) return result;

      lua_pop(L, 1); // clean up value reference
    }
    if ((result = yajl_gen_map_close(h)) != yajl_gen_status_ok) return result;
  }

  return yajl_gen_status_ok;
}

static int json_encode(lua_State *L) {
  int val_type = lua_type(L, 1);
  if (val_type == LUA_TFUNCTION ||
      val_type == LUA_TUSERDATA ||
      val_type == LUA_TTHREAD   ||
      val_type == LUA_TLIGHTUSERDATA) {
    lua_pushliteral(L, "cannot encode opaque type");
    lua_error(L);
    return -1;
  }

  yajl_gen h = yajl_gen_alloc(NULL);
  if (h == NULL) {
    lua_pushliteral(L, "could not initialize JSON encoder");
    lua_error(L);
    return -1;
  }

  yajl_gen_status result = json_encode_helper(h, L, 1);
  if (result != yajl_gen_status_ok) {
    lua_pushfstring(L, "failed to encode JSON: YAJL status %d", result);
    lua_error(L);
    return -1;
  }

  const unsigned char *encoded;
  size_t encoded_len;

  yajl_gen_get_buf(h, &encoded, &encoded_len);
  lua_pushlstring(L, (const char*) encoded, encoded_len);
  yajl_gen_clear(h);
  yajl_gen_free(h);

  return 1;
}

static int json_decode(lua_State *L) {
  if ( ! lua_isstring(L, 1)) {
    lua_pushliteral(L, "cannot decode non-string");
    lua_error(L);
    return -1;
  }

  size_t jsonstr_len = 0;
  const unsigned char *jsonstr =
      (const unsigned char*) lua_tolstring(L, 1, &jsonstr_len);

  yajl_handle h = yajl_alloc(&json_parse_callbacks, NULL, (void*) L);
  if (h == NULL) {
    lua_pushliteral(L, "could not initialize JSON parser");
    lua_error(L);
    return -1;
  }

  yajl_config(h, yajl_allow_comments, 0);

  yajl_status result = yajl_parse(h, jsonstr, jsonstr_len);

  if (result != yajl_status_ok) {
    lua_pushfstring(L, "failed to decode JSON (YAJL: %s)",
        yajl_status_to_string(result));
    lua_error(L);
    return -1;
  }

  result = yajl_complete_parse(h);
  if (result != yajl_status_ok) {
    lua_pushfstring(L, "incomplete JSON (YAJL: %s)",
        yajl_status_to_string(result));
    lua_error(L);
    return -1;
  }

  yajl_free(h);
  return 1;
}

int luaopen_json(lua_State *L) {
  lua_createtable(L, 0, 1);

  lua_pushcfunction(L, json_decode);
  lua_setfield(L, -2, "decode");

  lua_pushcfunction(L, json_encode);
  lua_setfield(L, -2, "encode");

  return 1;
}

/* vim: set ts=2 sts=2 et sw=2: */
