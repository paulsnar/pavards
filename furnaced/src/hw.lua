local gpioc = require('gpioc')

local exports = {
  SENSOR_TEMP_FURNACE   = 0,
  SENSOR_TEMP_WH        = 1,
  SENSOR_TEMP_REVERSE   = 2,
  SENSOR_TEMP_LARGELOOP = 3,

  SWITCH_PUMP         = 17,
  SWITCH_WATERHEATER  = 22,
  SWITCH_BOILER       = 27,
}

local methods = {
  PING          = '?',
  VERSION       = 'V',
  MCP3008_READ  = 'i',
  MAX6955_READ  = 'r',
  MAX6955_WRITE = 'R',
  PIN_SET       = ':',
}

function exports.read_mcp3008(input)
  local rsp = gpioc.request({
    method = methods.MCP3008_READ,
    data = { input },
  })
  return rsp[1]
end

function exports.set_switch(id, state)
  gpioc.request({
    method = methods.PIN_SET,
    data = { id, state and 1 or 0 },
  })
end

return exports
