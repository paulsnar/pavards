local exports = { }

function exports.structural_eq(a, b, ilen)
  local type_a, type_b = type(a), type(b)
  if type_a ~= type_b then
    return false
  end
  if type_a ~= 'table' then
    -- we do the best we can
    return a == b
  end

  if ilen ~= nil then
    for i = 1, ilen do
      local va, vb = a[i], b[i]
      if not exports.structural_eq(va, vb) then return false end
    end
  else
    local keys = { }
    for k, v in pairs(a) do
      local val_a, val_b = v, b[k]
      if not exports.structural_eq(val_a, val_b) then return false end
      keys[k] = true
    end
    for k, v in pairs(b) do
      if keys[k] ~= true then
        return false
      end
    end
  end

  return true
end

local _bind_argcache = { }
function exports.bind(func, ...)
  local args, nargs = {...}, select('#', ...)

  for k, func in pairs(_bind_argcache) do
    if exports.structural_eq(args, k, nargs) then
      return func
    end
  end

  local bound = function(...)
    local moreargs, nmoreargs = {...}, select('#', ...)
    local sumargs = { }
    for i = 1, nargs do
      sumargs[i] = args[i]
    end
    for i = 1, nmoreargs do
      sumargs[i + nargs] = moreargs[i]
    end
    return func(table.unpack(sumargs, 1, nargs + nmoreargs))
  end
  _bind_argcache[args] = bound

  return bound
end

function exports.key_remap(array, keymap)
  local newdata = { }
  for i, v in ipairs(array) do
    local newv = { }
    for k, val in v do
      newv[keymap[k]] = val
    end
    newdata[i] = newv
  end
  return newdata
end

-- Roughly equal to the builtin Minetest dumper
-- Not intended for serialization, but works fine
local function dumpstr(val, indent, nested, level)
  local t = type(val)
  if t == 'number' or t == 'boolean' then
    return tostring(val)
  elseif t == 'nil' then
    return t
  elseif t == 'string' then
    return string.format('%q', val)
  elseif t ~= 'table' then
    return string.format('<%s>', t)
  end

  nested = nested or { }
  if nested[val] then return '<circular>' end
  nested[val] = true

  level  = level  or 1
  indent = indent or '  '

  local t, seen_num = { }, { }

  for i, v in ipairs(val) do
    table.insert(t, dumpstr(v, indent, nested, level + 1))
    seen_num[i] = true
  end
  for k, v in pairs(val) do
    if not seen_num[k] then
      if type(k) ~= 'string' then
        k = '[' .. dumpstr(k, indent, nested, level + 1) .. ']'
      end
      v = dumpstr(v, indent, nested, level + 1)
      table.insert(t, k .. ' = ' .. v)
    end
  end
  nested[val] = nil

  if indent ~= '' then
    local indent_str = '\n' .. string.rep(indent, level)
    local end_indent_str = '\n' .. string.rep(indent, level - 1)
    return string.format('{%s%s%s}',
        indent_str, table.concat(t, ',' .. indent_str), end_indent_str)
  end
  return '{' .. table.concat(t, ', ') .. '}'
end
exports.dumpstr = dumpstr

local function copy(val, ignorefailures)
  local igf, t = ignorefailures, type(val)
  if  t == 'number' or
      t == 'boolean' or
      t == 'string' or
      t == 'nil' then
    return t -- value references (technically)
  elseif t == 'table' then
    local newtable = { }
    for k, v in pairs(val) do
      newtable[copy(k, igf)] = copy(v, igf)
    end
    return newtable
  else
    if not igf then error('could not copy type') end
  end
end
exports.copy = copy

local function multiplex(...)
  local n = select('#', ...)
  local funcs, args = { }, {...}
  for i = 1, n do
    local func = args[i]
    table.insert(funcs, func)
  end
  local function multiplexed(...)
    local retval
    for i = 1, #funcs do
      retval = table.pack(funcs[i](...))
    end
    return table.unpack(retval, 1, retval.n)
  end
  return multiplexed
end
exports.multiplex = multiplex

return exports
