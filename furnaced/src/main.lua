local aux, cloud, hw, logger, scatter, sched, util, wallclock

aux       = require('aux')
cloud     = require('cloud')
hw        = require('hw')
logger    = require('log')
scatter   = require('structs.scatter')
sched     = require('scheduler')
util      = require('util')
wallclock = require('wallclock')

local log = logger.bound('main')

local config = {
  thresholds = {
    pump = {
      rise = 35,
      fall = { ll_open = 35, ll_closed = 60 },
    },

    boiler_queued       = 45,
    waterheater_queued  = 45,
  },
  resolution = 5,
  slow_resolution = 2.5,
  ambient_minimum = 45,
  skip_pump_logic_until = nil,

  -- TODO: implement a sliding scale for this depending on time-of-year
  rising_tendency_interval = 30 * wallclock.MINUTE,
  dropping_tendency_interval = 60 * wallclock.MINUTE,
  average_interval = 5 * wallclock.MINUTE,
  cleanup_interval = 1 * wallclock.HOUR,
}

local function adjust_degrees(reading)
  -- very rough estimate but it should work
  -- ASSUMES: reference voltage = +3.3 V
  --          temperature scale similar to the one in the datasheet
  local voltage = (reading / 1024) * 3.3
  return ((voltage / 1.75) * 175) - 44
end

local temp_sensors = {
  furnace     = { id =   hw.SENSOR_TEMP_FURNACE, history = scatter.create() },
  waterheater = { id =        hw.SENSOR_TEMP_WH, history = scatter.create() },
  reverse     = { id =   hw.SENSOR_TEMP_REVERSE, history = scatter.create() },
  largeloop   = { id = hw.SENSOR_TEMP_LARGELOOP, history = scatter.create() },
}
-- TODO DEBUG REMOVE ME IN PRODUCTION
-- temp_sensors.furnace.history.verbose = true
local switches = {
  pump        = { id = hw.SWITCH_PUMP,        on = false },
  waterheater = { id = hw.SWITCH_WATERHEATER, on = false },
  boiler      = { id = hw.SWITCH_BOILER,      on = false },
}

local function set_switch(switch, state)
  local ok, err = pcall(hw.set_switch, switch.id, state)
  if not ok then
    logger.log('main.switch', logger.levels.WARN,
        'Setting switch %s failed: %s',
        switch.id, err)
    return false
  end

  switch.on = state
  switch.queued = nil
  return true
end

local function heuristic_largeloop(min)
  local ll_avg = temp_sensors.largeloop.history:average(min)
  local rev_avg = temp_sensors.reverse.history:average(min)
  if  ll_avg < config.ambient_minimum or
      rev_avg < config.ambient_minimum then return false end
    return math.abs(rev_avg - ll_avg) < (2 * config.resolution)
end

local function make_decision()
  local now = wallclock.now()
  local skip_pump_logic =
    config.skip_pump_logic_until and now < config.skip_pump_logic_until

  local furnace_avg =
    temp_sensors.furnace.history:average(now - config.average_interval)

  if switches.pump.on then
    local furnace_tend =
      temp_sensors.furnace.history:tendency(
          now - config.dropping_tendency_interval, now,
          config.resolution)

    if not scatter.tend('dropping', furnace_tend) then
      goto skip_furnace_temp_consideration
    end

    if not skip_pump_logic then
      local largeloop_on = heuristic_largeloop(now - config.average_interval)
      local off_threshold = config.thresholds.pump.fall[
          largeloop_on and 'll_open' or 'll_closed']

      if  scatter.tend('dropping', furnace_tend) and
          furnace_avg < off_threshold then
        set_switch(switches.pump, false)
      end
    end

    if  switches.boiler.queued and
        furnace_avg < config.thresholds.boiler_queued then
      set_switch(switches.boiler, true)
    end

    if  switches.waterheater.queued and
        furnace_avg < config.thresholds.waterheater_queued then
      set_switch(switches.waterheater, true)
    end
  else
    local furnace_tend =
      temp_sensors.furnace.history:tendency(
          now - config.rising_tendency_interval, now,
          config.resolution)

    if  scatter.tend('rising', furnace_tend) and
        furnace_avg > config.thresholds.pump.rise then
      set_switch(switches.pump, true)
    end
  end

  ::skip_furnace_temp_consideration::
end

local function refresh_switches()
  local now = wallclock.now()

  local ok, newswitches = pcall(cloud.get_switches, cloud)
  if not ok then
    log(logger.levels.ERROR, 'cloud.get_switches failed: %s', newswitches)
    return
  end

  if newswitches.wh == 'on' or newswitches.wh == 'off' then
    set_switch(switches.waterheater, newswitches.wh == 'on')
  elseif newswitches.wh == 'queue' then
    switches.waterheater.queued = true
  end

  if newswitches.boiler == 'on' or newswitches.boiler == 'off' then
    set_switch(switches.boiler, newswitches.boiler == 'on')
  elseif newswitches.boiler == 'queue' then
    switches.boiler.queued = true
  end

  if newswitches.pump == 'on' or newswitches.pump == 'off' then
    local ok = set_switch(switches.pump, newswitches.pump == 'on')
    if ok then
      config.skip_pump_logic_for = now + (5 * wallclock.MINUTE)
    end
  end
end

local function refresh_temperatures()
  local now = wallclock.now()

  for _, sensor in pairs(temp_sensors) do
    local ok, reading = pcall(hw.read_mcp3008, sensor.id)
    if ok then
      local degrees = adjust_degrees(reading)
      sensor.history:insert(now, degrees)
    else
      log(logger.levels.WARN, 'Obtaining temperature failed: %s', reading)
    end
  end
end

local function cleanup()
  local now = wallclock.now()
  local min = now - config.cleanup_interval
  for _, v in pairs(temp_sensors) do v.history:trim(min) end
end

local function run()
  log(logger.levels.INFO, 'Bootstrapping.')
  -- set up
  local scheduler = sched.create()

  -- scheduler:interval(refresh_thresholds, 5 * time.MINUTE, true)
  scheduler:interval(    refresh_switches, 75 * wallclock.SECOND, true)
  scheduler:interval(refresh_temperatures, 30 * wallclock.SECOND, true)

  scheduler:idle(cleanup)
  scheduler:idle(make_decision)

  -- do the loop
  while true do
    local sleep_for = scheduler:tick()
    --[[
    log(logger.levels.DEBUG,
      'Scheduler idle for %.2f seconds, sleeping...', sleep_for)
    --]]
    aux.sleep(sleep_for)
    --log(logger.levels.DEBUG, 'Ticking')
  end
end

return run
