local timeseries, wallclock =
  require('structs.timeseries'),
  require('wallclock')

local exports = { }
local Scheduler = { }

function exports.create()
  local instance = {
    queue     = timeseries.create(),
    idle_cbs  = { },
  }

  for name, func in pairs(Scheduler) do
    instance[name] = func
  end

  return instance
end

function Scheduler:immediate(callback)
  self.queue:insert(-1, { callback = callback })
end

function Scheduler:timeout(callback, delta)
  local now = wallclock.now()
  self.queue:insert(now + delta, { callback = callback })
end

function Scheduler:at(callback, timestamp)
  self.queue:insert(timestamp, { callback = callback })
end

function Scheduler:idle(callback)
  table.insert(self.idle_cbs, callback)
end

function Scheduler:interval(callback, delta, immediate)
  local now
  if immediate then
    now = -delta
  else
    now = wallclock.now()
  end
  local intv = {
    callback = callback,
    interval = delta,
  }
  self.queue:insert(now + delta, intv)
end

function Scheduler:tick()
  local now, remerge = wallclock.now() + 1e-6, { }
  for key, obj in self.queue:iterate({ max = now, remove = true }) do
    local ret = obj.callback()
    if obj.interval ~= nil and ret ~= false then
      table.insert(remerge, { k = now + obj.interval, v = obj })
    end
  end

  for i = 1, #remerge do
    local o = remerge[i]
    self.queue:insert(o.k, o.v)
    remerge[i] = nil
  end

  local next = self.queue.first.key
  if next <= now then
    return self:tick()
  else
    for i = 1, #self.idle_cbs do
      self.idle_cbs[i]()
    end
    return next - now
  end
end

return exports
