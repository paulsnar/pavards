local util = require('util')

local exports = { }

local levels = {
  DEBUG     = 1,
  INFO      = 2,
  WARN      = 3,
  ERROR     = 4,
}
exports.levels = levels

-- global defaults
exports.current_level = levels.INFO
exports.colors = true

-- https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
local BASE = {
  mod_off    = 20,
  foreground = 30,
  background = 40,
  bright     = 60,
}
local COLORS = {
  black   = 0,
  red     = 1,
  green   = 2,
  yellow  = 3,
  blue    = 4,
  magenta = 5,
  cyan    = 6,
  white   = 7,
}
local MODIFIERS = {
  reset     = 0,
  bright    = 1,
  underline = 4,
  inverse   = 7,
}
local function esc_seq(mods)
  return '\x1b[' .. table.concat(mods, ';') .. 'm'
end

local function wrap_color(descr, string)
  local mods = { }

  if bold then
    table.insert(mods, MODIFIERS.bright)
    descr.fgbold = false
    descr.bgbold = false
  end
  if underline then
    table.insert(mods, MODIFIERS.underline)
  end

  local fg, bg = descr.fg, descr.bg

  if fg == 'black' then
    table.insert(mods, BASE.foreground + COLORS.black)
  elseif fg == 'red' then
    table.insert(mods, BASE.foreground + COLORS.red)
  elseif fg == 'green' then
    table.insert(mods, BASE.foreground + COLORS.green)
  elseif fg == 'yellow' then
    table.insert(mods, BASE.foreground + COLORS.yellow)
  elseif fg == 'blue' then
    table.insert(mods, BASE.foreground + COLORS.blue)
  elseif fg == 'magenta' then
    table.insert(mods, BASE.foreground + COLORS.magenta)
  elseif fg == 'cyan' then
    table.insert(mods, BASE.foreground + COLORS.cyan)
  elseif fg == 'white' then
    table.insert(mods, BASE.foreground + COLORS.white)
  end
  if descr.fgbold then
    if mods[#mods] then
      mods[#mods] = mods[#mods] + BASE.bright
    end
  end

  if bg == 'black' then
    table.insert(mods, BASE.background + COLORS.black)
  elseif bg == 'red' then
    table.insert(mods, BASE.background + COLORS.red)
  elseif bg == 'green' then
    table.insert(mods, BASE.background + COLORS.green)
  elseif bg == 'yellow' then
    table.insert(mods, BASE.background + COLORS.yellow)
  elseif bg == 'blue' then
    table.insert(mods, BASE.background + COLORS.blue)
  elseif bg == 'magenta' then
    table.insert(mods, BASE.background + COLORS.magenta)
  elseif bg == 'cyan' then
    table.insert(mods, BASE.background + COLORS.cyan)
  elseif bg == 'white' then
    table.insert(mods, BASE.background + COLORS.white)
  end
  if descr.bgbold then
    if mods[#mods] then
      mods[#mods] = mods[#mods] + BASE.bright
    end
  end

  if descr.noreset then
    return esc_seq(mods) .. string
  end
  return esc_seq(mods) .. string .. esc_seq({ MODIFIERS.reset })
end

function log_format(level, module, format, ...)
  -- hello modulog :)
  local out

  if level == levels.ERROR then
    out = wrap_color({ fg = 'red', fgbold = true, noreset = true }, '[X] ')
    out = out .. wrap_color({ fg = 'red', noreset = true }, module)
  elseif level == levels.WARN then
    out = wrap_color({ fg = 'yellow', fgbold = true, noreset = true }, '[!] ')
    out = out .. wrap_color({ fg = 'yellow', noreset = true }, module)
  elseif level == levels.INFO then
    out = wrap_color({ fg = 'cyan', fgbold = true, noreset = true }, '[i] ')
    out = out .. wrap_color({ fg = 'cyan', noreset = true }, module)
  elseif level == levels.DEBUG then
    out = wrap_color({ fg = 'white', noreset = true }, '[#] ')
    out = out .. wrap_color({ fg = 'black', fgbold = true, noreset = true }, module)
  else
    out = wrap_color({ fg = 'magenta' }, '[?] ')
    out = out .. module
  end
  out = out .. wrap_color({ fg = 'blue', fgbold = true }, os.date(' %Y-%m-%d %H:%M:%S'))
  
  format = '%s ' .. format .. '\n'

  local ok, str = pcall(string.format, format, out, ...)
  if not ok then
    return out .. ' (format error: ' .. str .. ')\n'
  end
  return str
end

function exports.log(module, level, ...)
  if level < exports.current_level then return false end
  local str = log_format(level, module, ...)
  io.stdout:write(str)
end

function exports.bound(module)
  return util.bind(exports.log, module)
end

return exports
