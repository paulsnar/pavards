local logger, util = require('log'), require('util')

local exports = { }
local Scatter = { }

function pearson(points)
  local len = #points

  local avg_x, avg_y = 0, 0
  for i = 1, len do
    local point = points[i]
    avg_x = avg_x + point.x
    avg_y = avg_y + point.y
  end
  avg_x = avg_x / len
  avg_y = avg_y / len

  local std_x, std_y = 0, 0
  for i = 1, len do
    local point = points[i]
    std_x = std_x + math.pow(point.x - avg_x, 2)
    std_y = std_y + math.pow(point.y - avg_y, 2)
  end
  std_x = math.sqrt(std_x / (len - 1))
  std_y = math.sqrt(std_y / (len - 1))

  local std_prod = 0
  for i = 1, len do
    local point = points[i]
    local str_x, str_y =
      (point.x - avg_x) / std_x,
      (point.y - avg_y) / std_y
    std_prod = std_prod + (str_x * str_y)
  end
  std_prod = std_prod / (len - 1)

  if std_prod ~= std_prod then
    -- This is a sign that we're dealing with a NaN, e.g., the Pearson
    -- formula has determined that this series has no correlation.
    -- From a technical standpoint, NaN is correct, but practically
    -- the result should be zero, so we normalize it here.
    -- BTW NaN is good in theory, but the above inequality seems to make
    -- no sense if you don't know the semantics of IEEE 754. It would be
    -- great if Lua would provide, e.g. math.isnan (like JS does),
    -- but oh well.
    return 0
  end
  return std_prod
end


function exports.tend(how, tendency)
  if how == 'dropping' then
    return tendency < -0.75
  elseif how == 'rising' then
    return tendency > 0.75
  else
    return nil
  end
end

function exports.create()
  local instance = {
    keys = { },
    data = { },
  }

  for name, func in pairs(Scatter) do
    instance[name] = func
  end

  return instance
end

function Scatter:insert(x, y)
  if #self.keys > 0 then
    for i = 1, #self.keys do
      local x_prev, x_next = self.keys[i], self.keys[i + 1]
      if x_next == nil then
        table.insert(self.keys, x)
      elseif x_prev <= x and x < x_next then
        table.insert(self.keys, i + 1, x)
      end
    end
  else
    table.insert(self.keys, x)
  end
  self.data[x] = y
end

function Scatter:tendency(x_min, x_max, delta_min)
  if x_min == nil then x_min = -math.huge end
  if x_max == nil then x_max =  math.huge end
  if delta_min == nil then delta_min = 0 end

  local sum, points, y_min, y_max = 0, { }, math.huge, -math.huge
  local y_vals

  if self.verbose then y_vals = { } end

  for i, x in ipairs(self.keys) do
    if x < x_min then goto continue end
    if x_max <= x then break end

    local y = self.data[x]
    table.insert(points, { x = x, y = y })
    sum = sum + y
    if y < y_min then y_min = y end
    if y_max < y then y_max = y end

    if y_vals then table.insert(y_vals, y) end

    ::continue::
  end

  if self.verbose then
    logger.log('str.scatter', logger.levels.DEBUG,
        '-- STATE DUMP --\n\tmethod: tendency(x_min=%f, x_max=%f)\n' ..
          '\tvalue sum = %f\n\tprocessed values = %s\n\traw keys %s\n\traw data %s',
        x_min, x_max,
        sum,
        util.dumpstr(y_vals, ''),
        util.dumpstr(self.keys, ''),
        util.dumpstr(self.data, ''))
  end

  if y_max - y_min < delta_min then
    -- points too close to a line to be sure!
    return 0, sum / #points
  end
  return pearson(points), sum / #points
end

function Scatter:average(x_min, x_max)
  if x_min == nil then x_min = -math.huge end
  if x_max == nil then x_max =  math.huge end

  local sum, n = 0, 0
  for _, x in ipairs(self.keys) do
    if x < x_min then goto continue end
    if x_max <= x then break end

    local y = self.data[x]
    sum = sum + y
    n = n + 1

    ::continue::
  end

  if self.verbose then
    logger.log('str.scatter', logger.levels.DEBUG,
        '-- STATE DUMP --\n\tmethod: average(x_min=%f, x_max=%f)\n' ..
          '\tvalue sum = %f\n\traw keys %s\n\traw data %s',
        x_min, x_max,
        sum,
        util.dumpstr(self.keys, ''),
        util.dumpstr(self.data, ''))
  end

  return sum / n
end

function Scatter:min()
  return self.keys[1], self.data[self.keys[1]]
end

function Scatter:max()
  return self.keys[#self.keys], self.data[self.keys[#self.keys]]
end

function Scatter:trim(x_min, x_max)
  if x_min == nil then x_min = -math.huge end
  if x_max == nil then x_max =  math.huge end

  local min_i, max_i, initial_len = nil, nil, #self.keys
  for i = 1, #self.keys do
    local x = self.keys[i]
    if x <= x_min then
      min_i = i + 1
      self.keys[i] = nil
      self.data[x] = nil
    elseif x_max < x then
      if max_i == nil then max_i = i - 1 end
      self.keys[i] = nil
      self.data[x] = nil
    end
  end
  if min_i ~= nil and max_i == nil then
    max_i = initial_len
  elseif max_i ~= nil and min_i == nil then
    min_i = 1
  end
  if min_i ~= nil and max_i ~= nil then
    local newkeys = { }
    for i = min_i, max_i do
      table.insert(newkeys, self.keys[i])
    end
    self.keys = newkeys
  end

  if self.verbose then
    logger.log('str.scatter', logger.levels.DEBUG,
        '-- STATE DUMP --\n\tmethod: trim(x_min=%f, x_max=%f)\n' ..
          '\tremoved keys up to %d and from %d\n\traw keys %s\n\traw data %s',
        x_min, x_max,
        min_i or 0, max_i or 0,
        util.dumpstr(self.keys, ''),
        util.dumpstr(self.data, ''))
  end
end

return exports
