local util = require('util')

local exports = { }
local Timeseries = { }

function ll_relink(prev, mid, next)
  if next ~= nil then next.prev = mid end
  if prev ~= nil then prev.next = mid end
  mid.prev = prev
  mid.next = next
  return prev, mid, next
end

function ll_neuter(mid)
  local prev, next = mid.prev, mid.next
  if prev ~= nil then prev.next = next end
  if next ~= nil then next.prev = prev end
  mid.next = nil
  mid.prev = nil
  return mid
end

function exports.create()
  local instance = {
    first = nil,
    last = nil,
  }

  for name, func in pairs(Timeseries) do
    instance[name] = func
  end

  return instance
end

function Timeseries:insert(key, value)
  local item = { key = key, value = value }
  if self.first == nil then
    self.first = item
    self.last = item
    return
  elseif item.key < self.first.key then
    _, self.first, _ = ll_relink(nil, item, self.first)
  elseif self.last.key < item.key then
    _, self.last, _ = ll_relink(self.last, item, nil)
  else
    -- POSSIBLE OPTIMIZATION: decide whether to iterate
    -- from start or from end judging by key
    local iter = self.first
    while iter ~= nil do
      if  iter.key <= item.key and
          item.key < (iter.next or { key = math.huge }).key then
        ll_relink(iter, item, iter.next)
        return
      else
        iter = iter.next
      end
    end
  end
end

function timeseries_iterator(s, lastkey)
  local item = s.iter
  if item == nil then
    if lastkey == nil then
      item = s.self.first
    else
      return nil, nil
    end
  end

  if not s.continue(item.key) then
    return nil, nil
  end

  -- advance iterator state
  local next, continue = nil, false
  if item.next ~= nil then
    next, continue = item.next, s.continue(item.next.key)
  end

  if continue then
    s.iter = next
  else
    s.iter = nil
  end

  if s.destructive then
    if next == nil then
      -- item == s.self.last
      s.self.last = nil
    end
    if not continue then
      s.self.first = next
    end

    ll_neuter(item)
  end

  return item.key, item.value
end

function timeseries_iter_forever()
  return true
end

function timeseries_iter_minmax(min, max, key)
  return min <= key and key < max
end

function Timeseries:query(params)
  params = params or { }
  local min, max = params.min, params.max
  if min == nil then min = -math.huge end
  if max == nil then max =  math.huge end

  local results, item = { }, self.first
  while item ~= nil do
    if min <= item.key then
      if item.key < max then
        table.insert(results, item.value)
      else
        break
      end
    end
    item = item.next
  end

  return results
end

function Timeseries:iterate(params)
  params = params or { }
  local s = { self = self, iter = nil }

  if params.min ~= nil or params.max ~= nil then
    local min, max = params.min, params.max
    if min == nil then min = -math.huge end
    if max == nil then max =  math.huge end
    s.continue = util.bind(timeseries_iter_minmax, min, max)
  else
    s.continue = timeseries_iter_forever
  end

  if params.remove then s.destructive = true end

  return timeseries_iterator, s
end

function Timeseries:dump()
  local iter, d = self.first
  while iter ~= nil do
    if d == nil then
      d = '{ ('
    else
      d = d .. ', ('
    end
    d = d .. tostring(iter.key) .. ' => ' .. tostring(iter.value) .. ')'
    iter = iter.next
  end
  return d .. ' }'
end

return exports
