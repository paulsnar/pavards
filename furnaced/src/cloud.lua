-- DUMMY
local logger = require('log')
local log = logger.bound('cloud')

local exports = { }

function exports.get_switches()
  log(logger.levels.DEBUG, 'get_switches invoked')
  return { }
end

return exports
