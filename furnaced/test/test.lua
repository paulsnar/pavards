---- Module system hack
package.path  = string.gsub(package.path, '%./%?', './dist/?')
package.cpath = string.gsub(package.cpath, '%./%?', './dist/?')

local aux, logger, util
aux     = require('aux')
logger  = require('log')
util    = require('util')


---- Utilities
local REFERENCE_VOLTAGE = 3.3
local convert = {
  -- MCP3008
  mcp_to_v = function(reading) return (reading / 1024) * REFERENCE_VOLTAGE end,
  v_to_mcp = function(v) return math.floor((v / REFERENCE_VOLTAGE) * 1024) end,

  -- TMP36
  v_to_deg = function(v) return ((v / 1.75) * 175) - 44 end,
  deg_to_v = function(deg) return ((deg + 44) / 175) * 1.75 end,
}

local tmp = {
  furnace   = 0,
  wh        = 1,
  reverse   = 2,
  largeloop = 3,
}
local sw = {
  pump    = 17,
  wh      = 22,
  boiler  = 27,
}


---- Mocks
package.loaded.aux = {
  sleep = coroutine.yield,
}

local wallclock, wc_now = nil, 7500.0
package.loaded.wallclock = {
  NANOSECOND  = 1e-9,
  MICROSECOND = 1e-6,
  MILLISECOND = 1e-3,
  SECOND      = 1,
  MINUTE      = 60,
  HOUR        = 3600,
  now         = function()
    local wc = wc_now
    wc_now = wc_now + wallclock.NANOSECOND -- system calls take time
    return wc
  end,
}
wallclock = package.loaded.wallclock

local gpio_state = {
  mcp = { [0] = 0, [1] = 0, [2] = 0, [3] = 0 },
  pins = { [17] = false, [22] = false, [27] = false },
}
package.loaded.gpioc = {
  request = function(data)
    if data.method == 'i' then
      local sensor = data.data[1]
      if sensor < 0 or sensor > 7 then error('sensor out of bounds') end
      if sensor > 3 then
        return { 0, sequence = 0 }
      end
      local v = gpio_state.mcp[sensor]
      if v == nil then error('something went wrong') end
      -- logger.log('(mock) gpioc', logger.levels.DEBUG,
      --   'read MCP3008 input %d (at +%.3f V)', sensor, gpio_state.mcp[sensor])
      return { convert.v_to_mcp(v), sequence = 0 }
    elseif data.method == ':' then
      local pin, state = data.data[1], data.data[2]
      if pin ~= 17 and pin ~= 22 and pin ~= 27 then
        error('invalid pin triggered')
      end
      if state == 0 then
        logger.log('(mock) gpioc', logger.levels.INFO,
          'pin %d to off', pin)
        gpio_state.pins[pin] = false
      elseif state == 1 then
        logger.log('(mock) gpioc', logger.levels.INFO,
          'pin %d to on', pin)
        gpio_state.pins[pin] = true
      else
        error('invalid pin state')
      end

      return { state, sequence = 0 }
    elseif data.method == '?' then
      logger.log('(mock) gpioc', logger.levels.DEBUG,
        'ping %d', data.data[1])
      return { data.data[1], sequence = 0 }
    elseif data.method == 'V' then
      logger.log('(mock) gpioc', logger.levels.DEBUG, 'version')
      return { 1, sequence = 0 }
    else
      error('invalid gpioc method called')
    end
  end,
}

local cloud = {
  switch_queue = { },
}
package.loaded.cloud = {
  get_switches = function()
    local sw = table.remove(cloud.switch_queue, 1)
    if sw == nil then sw = { } end
    return sw
  end,
}

local FAUX_SIGMA = 1e-6
local function transition(from, target, steps, setter)
  local value, stepsize = from, (target - from) / steps
  while math.abs(value - target) > FAUX_SIGMA do
    value = value + stepsize
    if setter then setter(value) end
    coroutine.yield(value)
  end
  while true do
    if setter then setter(target) end
    coroutine.yield(target)
  end
end

local function test_behaviour(name, main, maxtime, incrementor, condition)
  local offset = 0
  while offset < maxtime do
    local ok, result = coroutine.resume(main)
    if not ok then
      logger.log('test.behaviour', logger.levels.ERROR,
          '%s: failed!\n\t%s', name, result)
      return false
    end
    wc_now = wc_now + result
    offset = offset + result

    if incrementor then
      local ok, err = pcall(incrementor)
      if not ok then
        logger.log('test.behaviour', logger.levels.WARN,
            '%s: incrementor failed!\n\t%s', name, err)
      end
    end
  
    if condition then
      local ok, test = pcall(condition)
      if not ok then
        logger.log('test.behaviour', logger.levels.ERROR,
            '%s: condition failed!\n\t%s', name, test)
        return false
      elseif test == true then
        if offset < maxtime then
          logger.log('test.behaviour', logger.levels.WARN,
              '%s: behaviour finished early (%.1f/%.1f s)',
              name, offset, maxtime)
        end
        return true
      end
    end
  end 
  if condition and not condition() then
    logger.log('test.behaviour', logger.levels.ERROR,
        '%s: failed!\n\t%s', name, 'end reached but condition unfulfilled')
    return false
  else
    return true
  end
end

local function run()
  logger.current_level = logger.levels.DEBUG
  local log = logger.bound('[test]')

  local main = coroutine.create(require('main'))
  gpio_state.mcp[0] = convert.deg_to_v(20)
  gpio_state.mcp[1] = convert.deg_to_v(35)
  gpio_state.mcp[2] = convert.deg_to_v(20)
  gpio_state.mcp[3] = convert.deg_to_v(20)

  local function mcp_set(n, v) gpio_state.mcp[n] = v end
  local mcp_setters = {
    [0] = util.bind(mcp_set, 0),
    [1] = util.bind(mcp_set, 1),
    [2] = util.bind(mcp_set, 2),
    [3] = util.bind(mcp_set, 3),
  }

  local incrementor, maxtime

  maxtime = 25 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
      convert.deg_to_v(20), convert.deg_to_v(40),
      maxtime // 30 + 11, mcp_setters[0])
  local ok = test_behaviour('simple (1/3 rise)',
      main, maxtime, incrementor,
      function() return gpio_state.pins[17] end)
  if not ok then os.exit(1) end
  log(logger.levels.INFO, 'simple (1/3 rise) done')

  local ok = test_behaviour('simple (2/3 plateau)',
      main, 45 * wallclock.MINUTE + 1, nil, nil)
  if not ok then os.exit(1) end
  log(logger.levels.DEBUG, 'simple (2/3 plateau) done')

  --[=[ This one is slightly incorrect, because it assumes that the drop
        temperature is around the same as the rise temperature, but it
        actually moreso tests the implementation of the correlation
        detection. Nonetheless, it is good as a sanity check, albeit
        should be tweaked when the algorithm changes. ]=]--
  maxtime = 45 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
      convert.deg_to_v(40), convert.deg_to_v(33),
      maxtime // 30 + 10, mcp_setters[0])
  local ok = test_behaviour('simple drop',
      main, maxtime, incrementor,
      function() return not gpio_state.pins[17] end)
  if not ok then os.exit(1) end
  log(logger.levels.INFO, 'simple (3/3 drop) done')
  

  --[=[ The following behaviour suite works with basically the same assumption
        as mentioned in the previous comment. It might fail (or start warning)
        if the correlation algorithm is tweaked elsewhere, so be sure to tweak
        the test as well. ]=]--

  maxtime = 25 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
      convert.deg_to_v(33), convert.deg_to_v(40),
      maxtime // 30 + 17, mcp_setters[0])
  local ok = test_behaviour('switch queue (1/3 rise)',
      main, maxtime, incrementor,
      function() return gpio_state.pins[17] end)
  log(logger.levels.INFO, 'switch queue (1/3 rise) done')

  maxtime = 45 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
      convert.deg_to_v(40), convert.deg_to_v(60),
      maxtime // 30, mcp_setters[0])
  local ok = test_behaviour('switch queue (2/3 plateau)',
      main, 45 * wallclock.MINUTE + 1, incrementor, nil)
  if not ok then os.exit(1) end
  if not gpio_state.pins[17] then
    log(logger.levels.ERROR, 'switch queue (2/3 plateau): pump switched off!')
    os.exit(1)
  end
  log(logger.levels.DEBUG, 'switch queue (2/3 plateau) done')

  table.insert(cloud.switch_queue, { wh = 'queue', boiler = 'queue' })
  maxtime = 45 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
      convert.deg_to_v(60), convert.deg_to_v(30),
      maxtime // 30 + 10, mcp_setters[0])
  local ok = test_behaviour('switch queue (3/3 drop)',
      main, maxtime, incrementor,
      function() return (not gpio_state.pins[17]) and
          gpio_state.pins[22] and gpio_state.pins[27] end)
  if not gpio_state.pins[22] then
    log(logger.levels.ERROR, 'switch queue (3/3 drop): pin 22 off!')
    os.exit(1)
  elseif not gpio_state.pins[27] then
    log(logger.levels.ERROR, 'switch queue (3/3 drop): pin 27 off!')
    os.exit(1)
  else
    log(logger.levels.INFO, 'switch queue (3/3 drop) done')
  end

  table.insert(cloud.switch_queue, { wh = 'off', boiler = 'off' })
  local ok = test_behaviour('switch queue (4/3 cleanup)',
      main, 5 * wallclock.MINUTE + 1, nil, nil)
  if not ok then os.exit(1) end
  log(logger.levels.DEBUG, 'switch queue cleanup done')


  --[=[ Okay, now here's the meat, as some would say! Here we begin actual
        testing of satisfaction of the pump off threshold when met with a
        large-loop-open condition.

        Essentially, when the large loop is closed, we want to switch off the
        pump as soon as possible, because the heat losses are much more
        minimal, and if the water gets too cold, it starts to also cool the
        water left in the water heater, which means that the average
        temperature of the water in the water heater will be too low to be
        useful. ]=]--

  mcp_setters[3](convert.deg_to_v(20))

  maxtime = 27 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
    convert.deg_to_v(20), convert.deg_to_v(40),
    maxtime // 20, mcp_setters[0])
  local ok = test_behaviour('shortloop (1/3)',
      main, maxtime, incrementor,
      function() return gpio_state.pins[17] end)
  if not ok then os.exit(1) end
  log(logger.levels.INFO, 'shortloop(1/3) done')

  maxtime = 30 * wallclock.MINUTE + 1
  local inc1, inc2 = coroutine.wrap(transition), coroutine.wrap(transition)
  inc1(
    convert.deg_to_v(40), convert.deg_to_v(50),
    maxtime // 30, mcp_setters[0])
  inc2(
    convert.deg_to_v(20), convert.deg_to_v(50),
    maxtime // 30, mcp_setters[3])
  incrementor = util.multiplex(inc1, inc2)
  test_behaviour('shortloop (2/3)',
    main, maxtime, incrementor, nil)
  log(logger.levels.DEBUG, 'shortloop(2/3) done')

  maxtime = 30 * wallclock.MINUTE + 1
  incrementor = coroutine.wrap(transition)
  incrementor(
    convert.deg_to_v(50), convert.deg_to_v(25),
    maxtime // 30, util.multiplex(mcp_setters[0], mcp_setters[3]))
  local ok = test_behaviour('shortloop (3/3)',
      main, wallclock.MINUTE // 2, incrementor,
      function() return gpio_state.pins[17] end)
  if not ok then os.exit(1) end
  log(logger.levels.INFO, 'shortloop(3/3) done')
end

return run
