local log = require('log')

log.current_level = -1

log.log('log-test',                0, 'Hello, %s!', 'world')
log.log('log-test', log.levels.DEBUG, 'Hello, %s!', 'world')
log.log('log-test',  log.levels.INFO, 'Hello, %s!', 'world')
log.log('log-test',  log.levels.WARN, 'Hello, %s!', 'world')
log.log('log-test', log.levels.ERROR, 'Hello, %s!', 'world')
