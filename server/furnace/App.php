<?php

namespace Furnace;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\EventDispatcher\EventDispatcher;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;

use Furnace\API\Version0\Controller as APIv0Controller;
use Furnace\App\Container;
use Furnace\App\Event\PrefilterListener;

class App
{
  protected static $instance;

  protected static function singleton()
  {
    if (self::$instance === null) {
      self::$instance = new App();
    }
    return self::$instance;
  }

  public static function run()
  {
    $app = self::singleton();
    return $app->handleFromGlobals();
  }

  public static function configure(array $conf)
  {
    $app = self::singleton();
    return $app->importConfiguration($conf);
  }

  protected $container;

  public function __construct()
  {
    $this->container = new Container();
    $this->registerRoutes();
    $this->registerListeners();
  }

  protected function registerRoutes()
  {
    $routeCollections = $this->container['Routes._all'];
    
    $routeSuperset = new RouteCollection();
    foreach ($routeCollections as $coll) {
      $routeSuperset->addCollection($coll);
    }

    $matcher = new UrlMatcher($routeSuperset, new RequestContext());
    $routerListener = new RouterListener($matcher, new RequestStack());

    $this->container['Dispatcher']->addSubscriber($routerListener);
  }

  protected function registerListeners()
  {
    $dispatcher = $this->container['Dispatcher'];

    $dispatcher->addSubscriber(new PrefilterListener());
  }

  public function importConfiguration(array $conf)
  {
    $configContainer = $this->container['Subcontainers.Config'];
    foreach ($conf as $key => $value) {
      $configContainer[$key] = $value;
    }
  }

  public function handleFromGlobals()
  {
    $kernel = $this->container['HttpKernel'];

    $req = Request::createFromGlobals();
    $resp = null;
    try {
      $resp = $kernel->handle($req);
    } catch (NotFoundHttpException $e) {
      $resp = new Response("Sorry, not found.\n", 404);
    } finally {
      if ($resp === null) {
        $resp = new Response("Sorry, something went wrong.\n", 500);
      }
      $resp->send();
      $kernel->terminate($req, $resp);
    }
  }
}
