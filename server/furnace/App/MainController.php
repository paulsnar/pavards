<?php

namespace Furnace\App;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

use Pimple\Container;

class MainController
{
  protected $container;
  protected $routes;
  protected $dispatcher;

  public function __construct(Container $c)
  {
    $this->container = $c;
    $this->dispatcher = $c['Dispatcher'];

    $this->routes = $this->createRouteCollection();
  }

  protected function createRouteCollection()
  {
    $routes = new RouteCollection();

    $routes->add('index', new Route('/', [
      '_controller' => [$this, 'indexAction'],
    ]));

    return $routes;
  }

  public function getRoutes() { return $this->routes; }

  public function indexAction(Request $req)
  {
    return new Response("Pavards\n", Response::HTTP_OK);
  }
}
