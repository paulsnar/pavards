<?php

namespace Furnace\App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class PrefilterListener implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      KernelEvents::CONTROLLER => [
        ['onControllerResolution', -1],
      ],
    ];
  }

  public function onControllerResolution(FilterControllerEvent $e)
  {
    $req = $e->getRequest();
    $attrs = $req->attributes;
    if ($attrs->has('_prefilter')) {
      $filters = $attrs->get('_prefilter');
      if ( ! is_callable($filters)) {
        foreach ($filters as $filter) {
          $result = $filter($req);
          if ($result !== null) {
            $e->stopPropagation();
            $e->setController(function () use ($result) {
              return $result;
            });
            return;
          }
        }
      } else {
        $result = $filters($req);
        if ($result !== null) {
          $e->stopPropagation();
          $e->setController(function () use ($result) {
            return $result;
          });
          return;
        }
      }
    }
  }
}
