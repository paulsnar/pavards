<?php

namespace Furnace\App;

use Symfony\Component\EventDispatcher\EventDispatcher;

use Symfony\Component\HttpFoundation\RequestStack;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

use Furnace\API\Version0\Controller as APIv0Controller;
use Furnace\API\Common\Container as APIContainer;
use Furnace\App\MainController;
use Furnace\Data\DatabaseConnection;
use Furnace\Data\LateSyncManager;
use Furnace\Utilities\RouteCollectionWrapper;

use Pimple\Container as PimpleContainer;

class Container extends PimpleContainer
{
  public function __construct()
  {
    parent::__construct();
    $this->injectEntries();
  }

  protected function injectEntries()
  {
    $this['Dispatcher'] = function ($c) {
      return new EventDispatcher();
    };

    $this['Controllers.Main'] = function ($c) {
      return new MainController($c);
    };

    $this['Controllers.API.v0'] = function ($c) {
      return new APIv0Controller($c);
    };

    $this['Routes._all'] = function ($c) {
      return [
        $c['Routes.Main'],
        $c['Routes.API.v0'],
      ];
    };

    $this['Routes.Main'] = function ($c) {
      return $c['Controllers.Main']->getRoutes();
    };
    $this['Routes.API.v0'] = function ($c) {
      return $c['Controllers.API.v0']->getRoutes();
    };

    $this['HttpKernel'] = function ($c) {
      $dispatcher = $c['Dispatcher'];

      $controllerResolver = new ControllerResolver();
      $argumentResolver = new ArgumentResolver();
      $rqStack = new RequestStack();

      return new HttpKernel(
          $dispatcher, $controllerResolver, $rqStack, $argumentResolver);
    };

    $this['LateSyncManager'] = function ($c) {
      $lsm = new LateSyncManager();
      $d = $c['Dispatcher'];
      $d->addSubscriber($lsm);
      return $lsm;
    };

    $this['Database'] = function ($c) {
      return new DatabaseConnection(
          $c['LateSyncManager'], $c['Subcontainers.Config']);
    };

    $this['Subcontainers.API'] = function ($c) {
      return new APIContainer($c);
    };

    $this['Subcontainers.Config'] = function ($c) {
      return new PimpleContainer();
    };
  }

}
