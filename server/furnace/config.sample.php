<?php

Furnace\App::configure([
  'db.driver' => 'sqlite3',
  'db.sqlite3.path' => FR_LIB_ROOT . DIRECTORY_SEPARATOR . 'furnace.db',
]);
