<?php

namespace Furnace\Utilities;

use Furnace\Utilities\Exception\Base32Exception;

/* http://www.crockford.com/wrmg/base32.html */

class Base32
{
  const FORWARD = [
    0 => '0',
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5',
    6 => '6',
    7 => '7',
    8 => '8',
    9 => '9',
    10 => 'a',
    11 => 'b',
    12 => 'c',
    13 => 'd',
    14 => 'e',
    15 => 'f',
    16 => 'g',
    17 => 'h',
    18 => 'j',
    19 => 'k',
    20 => 'm',
    21 => 'n',
    22 => 'p',
    23 => 'q',
    24 => 'r',
    25 => 's',
    26 => 't',
    27 => 'v',
    28 => 'w',
    29 => 'x',
    30 => 'y',
    31 => 'z',
  ];

  const BACKWARD = [
    '0' => 0, 'o' => 0,
    '1' => 1, 'i' => 1, 'l' => 1,
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    '7' => 7,
    '8' => 8,
    '9' => 9,
    'a' => 10,
    'b' => 11,
    'c' => 12,
    'd' => 13,
    'e' => 14,
    'f' => 15,
    'g' => 16,
    'h' => 17,
    'j' => 18,
    'k' => 19,
    'm' => 20,
    'n' => 21,
    'p' => 22,
    'q' => 23,
    'r' => 24,
    's' => 25,
    't' => 26,
    'v' => 27, 'u' => 27,
    'w' => 28,
    'x' => 29,
    'y' => 30,
    'z' => 31,
  ];

  public static function encode(string $bytestr)
  {
    $bytes = array_map(function ($byte) {
      return ord($byte);
    }, str_split($bytestr));
    $bytes_l = strlen($bytestr);

    // B32 requires that the byte count be divisible by 5
    // so we zero-pad the end of message
    if ($bytes_l % 5 !== 0) {
      $add = 5 - ($bytes_l % 5);
      for ($i = 0; $i < $add; $i++) {
        $bytes[] = 0;
      }
    }

    $char_indices = [ ];
    for ($i = 0; $i < $bytes_l; $i += 5) {
      $a = $bytes[$i];
      $b = $bytes[$i + 1];
      $c = $bytes[$i + 2];
      $d = $bytes[$i + 3];
      $e = $bytes[$i + 4];

      $s = ($a >> 3) & 0x1F;
      $t = ($a & 0x07) << 2;
      $t |= ($b >> 6) & 0x03;
      $u = ($b >> 1) & 0x1F;
      $v = ($b << 4) & 0x10;
      $v |= ($c >> 4) & 0x0F;
      $w = ($c << 1) & 0x1E;
      $w |= ($d >> 7) & 0x01;
      $x = ($d >> 2) & 0x1F;
      $y = ($d << 3) & 0x18;
      $y |= ($e >> 5) & 0x07;
      $z = $e & 0x1F;

      array_push($char_indices, $s, $t, $u, $v, $w, $x, $y, $z);
    }

    $chars = array_map(function ($i) {
      $v = self::FORWARD[$i] ?? null;
      if ($v === null) {
        throw new Base32Exception();
      }
      return $v;
    }, $char_indices);
    return implode('', $chars);
  }

  public static function decode(string $input)
  {
    $chars = str_split( strtolower($input) );
    $chars_l = count($chars);

    if ($chars_l % 8 !== 0) {
      $add = 8 - ($chars_l % 8);
      for ($i = 0; $i < $add; $i++) {
        $chars[] = '0';
      }
    }

    $charcodes = array_map(function ($i) {
      $v = self::BACKWARD[$i] ?? null;
      if ($v === null) {
        throw new Base32Exception();
      }
      return $v;
    }, $chars);

    $bytes = [ ];

    for ($i = 0; $i < $chars_l; $i += 8) {
      $a = $charcodes[$i];
      $b = $charcodes[$i + 1];
      $c = $charcodes[$i + 2];
      $d = $charcodes[$i + 3];
      $e = $charcodes[$i + 4];
      $f = $charcodes[$i + 5];
      $g = $charcodes[$i + 6];
      $h = $charcodes[$i + 7];

      $v = ($a << 3) | ($b >> 2);
      $w = (($b & 0x03) << 6) | ($c << 1) | (($d & 0x10) >> 4);
      $x = (($d & 0x0F) << 4) | (($e & 0x1E) >> 1);
      $y = (($e & 0x01) << 7) | ($f << 2) | (($g & 0x18) >> 3);
      $z = (($g & 0x07) << 5) | $h;

      array_push($bytes, $v, $w, $x, $y, $z);
    }

    $chars = array_map(function ($byte) {
      return chr($byte);
    }, $bytes);

    return implode('', $chars);
  }

  public static function fromHex(string $input)
  {
    return self::encode(hex2bin($input));
  }

  public static function toHex(string $input)
  {
    return bin2hex(self::decode($input));
  }
}
