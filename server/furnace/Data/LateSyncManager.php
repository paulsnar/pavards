<?php

namespace Furnace\Data;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class LateSyncManager implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      KernelEvents::TERMINATE => [
        'onKernelTermination',
      ],
    ];
  }

  protected $models;

  public function __construct()
  {
    $this->models = [ ];
  }

  public function onKernelTermination(PostResponseEvent $e)
  {
    foreach ($this->models as $model) {
      $model->sync();
    }
  }

  public function add(Model $m)
  {
    $this->models[] = $m;
  }
}
