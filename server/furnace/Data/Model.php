<?php

namespace Furnace\Data;

use Furnace\Data\Exception\ConflictException;
use Furnace\Data\Exception\DatabaseException;
use Furnace\Data\Exception\LogicException;

use \PDO;

class Model
{
  protected $db;
  protected $pristine;
  protected $attributes;
  protected $delta;
  protected $real;

  static $table;
  static $id_column = 'id';
  protected $id;
  static $version_column = 'version';
  protected $version;

  public function __construct(DatabaseConnection $db)
  {
    $this->db = $db;
    $this->pristine = true;
    $this->attributes = [ ];
    $this->delta = [ ];
    $this->real = false;

    $lsc = $this->db->getLateSyncManager();
    $lsc->add($this);
  }

  public function assign(array $attrs, bool $isLoad = false)
  {
    if ($isLoad) {
      $this->attributes = $attrs;
      $this->real = true;
    } else {
      $this->pristine = false;
      foreach ($attrs as $key => $value) {
        $this->delta[$key] = $value;
      }
    }
  }

  public function set(string $key, $value)
  {
    $this->pristine = false;
    $this->delta[$key] = $value;
  }

  public function get(string $key, $default = null)
  {
    if (array_key_exists($key, $this->delta)) {
      return $this->delta[$key];
    } else {
      return $this->attributes[$key];
    }
  }

  public function fetchVersion()
  {
    $table = $this->db->escapeIdentifier($this::$table);
    $id_col = $this->db->escapeIdentifier($this::$id_column);
    $ver_col = $this->db->escapeIdentifier($this::$version_column);
    $q = $this->db->prepareAndExecute(
        "SELECT $ver_col FROM $table WHERE $id_col = ?",
        [ 1 => $this->id ]);
    $result = $q->fetch(PDO::FETCH_NUM);
    if ($result === false) {
      return null;
    }
    return $result[0];
  }

  protected function nextVersion() { return $this->version + 1; }

  protected function remerge()
  {
    $table = $this->db->escapeIdentifier($this::$table);
    $id_col = $this->db->escapeIdentifier($this::$id_column);

    $q = $this->db->prepareAndExecute(
        "SELECT * FROM $table WHERE $id_col = ?",
        [ 1 => $this->id ]);
    $result = $q->fetch(PDO::FETCH_ASSOC);
    if ($result === false) {
      throw new DatabaseException($q->errorInfo());
    }

    $common = array_intersect_key($this->delta, $result);
    if (count($common) > 0) {
      throw new ConflictException();
    }

    return $this->delta;
  }

  public function sync()
  {
    if ( ! $this->real) {
      return $this->create();
    }
    if ( ! $this->pristine) {

      $this->db->beginTransaction();

      $currentVersion = $this->fetchVersion();

      if ($currentVersion !== $this->version) {
        // This will throw a ConflictException if a simple merge is impossible
        $this->remerge();
      }

      $colbinds = [ ];
      $valbinds = [ 0 => null ];
      foreach ($this->delta as $key => $value) {
        $colbinds[] = $this->db->escapeIdentifier($key) . ' = ?';
        $valbinds[] = $value;
      }
      unset($valbinds[0]);
      $table = $this->db->escapeIdentifier($this::$table);

      $colbinds[] = $this->db->escapeIdentifier($this::$version_column) . ' = ?';
      $valbinds[] = $this->nextVersion($this->version);

      $cols = implode(', ', $colbinds);

      $id_col = $this->db->escapeIdentifier($this::$id_column);
      $valbinds[] = $this->id;

      $this->db->prepareAndExecute(
          "UPDATE $table SET $cols WHERE $id_col = ?",
          $valbinds);

      $this->db->commit();

      // This overwrites attributes from delta. PHP array operators are weird
      $this->attributes = $this->delta + $this->attributes;
      $this->pristine = true;
    }
  }

  public function create()
  {
    if ($this->real) {
      return $this->sync();
    }

    $this->db->beginTransaction();

    $colbinds = [ ];
    $valbinds = [ 0 => null ];

    $attrs = $this->delta + $this->attributes;
    foreach ($attrs as $key => $value) {
      $colbinds[] = $this->db->escapeIdentifier($key);
      $valbinds[] = $value;
    }

    $colbinds[] = $this->db->escapeIdentifier($this::$version_column);
    $valbinds[] = $this->nextVersion();

    unset($valbinds[0]);
    $table = $this->db->escapeIdentifier($this::$table);

    $cols = implode(', ', $colbinds);
    $vals = str_repeat('?, ', count($valbinds));
    $vals = rtrim($vals, ', ');

    $this->db->prepareAndExecute(
        "INSERT INTO $table ($cols) VALUES ($vals)",
        $valbinds);

    $this->attributes = $this->delta + $this->attributes;
    $this->version = $this->nextVersion();
    
    $this->id = $this->db->lastInsertId($this::$id_column);
    if ($this->db->errorCode() === 'IM001') {
      // DB doesn't support ID returns! :(
      // Therefore this might be a bit buggy. Oh well, can't really
      // work around that though!
      $colbinds = [ ];
      $valbinds = [ 0 => null ];
      foreach ($this->attributes as $key => $value) {
        $colbinds[] = $this->db->escapeIdentifier($key) . ' = ?';
        $valbinds[] = $value;
      }
      unset($valbinds[0]);
      $cols = implode (' AND ', $colbinds);
      $id_col = $this->db->escapeIdentifier($this::$id_column);
      $ver_col = $this->db->escapeIdentifier($this::$version_column);
      $q = $this->db->prepareAndExecute(
          "SELECT $id_col, $ver_col FROM $table WHERE $cols",
          $valbinds);
      $res = $q->fetch(PDO::FETCH_ASSOC);
      if ($res === false) {
        // :/ //
        throw new ConflictException();
      }
      if ($res[$ver_col] !== $this->version) {
        throw new ConflictException();
      }
      $this->id = $res[$this::$id_column];
    }
    if (is_numeric($this->id)) {
      $this->id = intval($this->id);
    }

    $this->real = true;
    $this->pristine = true;
    $this->delta = [ ];

    $this->db->commit();
  }

  public function load($columnOverride = null, $idOverride = null)
  {
    $columns = $columnOverride;
    if ($columns === null) {
      $columns = '*';
    } else {
      $columns[] = $this::$version_column;
      $columns = array_map(function ($ident) {
        return $this->db->escapeIdentifier($ident);
      }, $columns);
      $columns = implode(', ', $columns);
    }

    $table = $this->db->escapeIdentifier($this::$table);

    if ($this->id === null && $idOverride === null) {
      throw new LogicException('No ID available for model');
    }

    $id = $idOverride;
    if ($id === null) {
      $id = $this->id;
    }
    $this->id = $id;

    $id_col = $this->db->escapeIdentifier($this::$id_column);

    $q = $this->db->prepareAndExecute(
        "SELECT $columns FROM $table WHERE $id_col = ?",
        [ 1 => $id ]);
    $result = $q->fetch(PDO::FETCH_ASSOC);
    if ($result === false) {
      if ($q->errorCode() === '00000') {
        // not found
        $this->attributes = [ ];
        $this->pristine = true;
        $this->delta = [ ];
        $this->real = false;
        return;
      }
      throw new DatabaseException($q->errorInfo());
    }

    $this->attributes = $result;
    if (array_key_exists($this::$id_column, $this->attributes)) {
      $this->id = $this->attributes[$this::$id_column];
      unset($this->attributes[$this::$id_column]);
    }
    if (is_numeric($this->id)) {
      $this->id = intval($this->id);
    }
    if (array_key_exists($this::$version_column, $this->attributes)) {
      $this->version = $this->attributes[$this::$version_column];
      unset($this->attributes[$this::$version_column]);
    }
    if (is_numeric($this->version)) {
      $this->version = intval($this->version);
    }

    $this->real = true;
    $this->pristine = true;
    $this->delta = [ ];
  }
}
