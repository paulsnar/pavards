<?php

namespace Furnace\Data;

use PDO;

use Pimple\Container;

use Furnace\Data\Exception\ConfigurationException;
use Furnace\Data\Exception\DatabaseException;

class DatabaseConnection extends PDO
{
  protected $lsm;
  protected $driver;

  public function __construct(LateSyncManager $lsm, Container $conf)
  {
    $driver = $conf['db.driver'];
    if ($driver !== 'sqlite3') {
      throw new ConfigurationException(
          sprintf('Unknown database driver specified: "%s"', $driver));
    }

    $filepath = $conf['db.sqlite3.path'];

    parent::__construct( sprintf('sqlite:%s', $filepath) );
    $this->driver = $driver;

    $this->lsm = $lsm;
  }

  public function prepareAndBind(string $query, array $params, array $types = [ ])
  {
    $q = $this->prepare($query);
    if ($q === false) {
      throw new DatabaseException($this->errorInfo());
    }

    foreach ($params as $key => $value) {
      if (array_key_exists($key, $types)) {
        $r = $q->bindValue($key, $value, $types[$key]);
      } else {
        $r = $q->bindValue($key, $value);
      }
      if ($r === false) {
        throw new DatabaseException($q->errorInfo());
      }
    }

    return $q;
  }

  public function prepareAndExecute(string $query, array $params, array $types = null)
  {
    if ($types !== null) {
      $q = $this->prepareAndBind($query, $params, $types);
    } else {
      $q = $this->prepareAndBind($query, $params);
    }

    $ok = $q->execute();
    if ($ok === false) {
      throw new DatabaseException($q->errorInfo());
    }

    return $q;
  }

  public function getLateSyncManager() { return $this->lsm; }


  private $__mysqlAnsiQuotesDisabled;
  public function escapeIdentifier(string $ident)
  {
    /*
      All of this technically abides to the SQL standard (except see below),
      but I wouldn't use this on untrusted input anyway. Be safe out there!
    */

    if ($this->driver === 'mysql') {
      /*
        For those who don't know, MySQL is a (bit of a) clusterfuck.

        The world has agreed that there exists this one standard, called SQL,
        upon which we should build database frontends. In case we need to
        have a funky table or column name (such as one which contains spaces),
        we surround such identifiers with particular characters, such as
        double quotes, as prescribed by the SQL Standard:

        > <delimited identifier> ::=
        >      <double quote> <delimited identifier body> <double quote>
        >
        > <delimited identifier body> ::= <delimited identifier part>...
        > 
        > <delimited identifier part> ::=
        >        <nondoublequote character>
        >      | <doublequote symbol>
        >
        > <doublequote symbol> ::= <double quote><double quote>

        The above quote is borrowed from SQL92 (section 5.2) [0]. Just to
        give you a timeframe of how long this has been the agreed-upon
        solution.

        So in 1995, three years after the above passage was written, a fancy
        new database engine called MySQL comes along. Since the dawn of time
        the creators of it have been acting as though what they use isn't
        *exactly* the standard, they just cherry-pick what looks nice and
        keep it mostly, but not completely, compatible with the standard.

        One such example is the identifier quoting mechanism. MySQL folks
        decided that there's not enough ways of specifying strings in SQL
        queries, so they made both single ('') and double ("") quotes
        valid data delimiters.

        But oh no, how should we now delimit identifiers? Well, there`s
        these `fancy` quotes, most often called backticks, and we`re not
        using them enough! So they decided on using `backticks` as
        identifier delimiters.

        Okay this, wouldn't be that bad, we could just add a special case
        as we have above and be done with it. Right, guys?

        Since then apparently some people have complained, because MySQL
        provides a couple of switchable sort-of "compatibility layers"
        for enabling or disabling various quirks of particular databases
        or of the SQL Standard. Among them is one called "ANSI_QUOTES",
        which makes MySQL identifiers abide by the rules set forth in
        the SQL Standard.

        But it's runtime-configurable, so essentially we have to check
        whether we are running in MySQL and then decide on the quoting
        strategy.

        Thanks, guys. You weren't trouble enough, apparently.

        P.S. The SQL dialect used by the Microsoft SQL Server also has
        a similar option ([1]), but instead it uses square brackets, and
        I can't find a reliable source telling how to escape those. But at
        least it's somewhat sane and defaults to supporting quotes.

        P.P.S. There's also SQLite, who, for compatibility reasons, support
        all three of the aforementioned forms for identifier escaping ([2]),
        except they've totally botched the square brackets, because none of
        the TSQL escape sequences I could find, work.

        [0]: https://www.contrib.andrew.cmu.edu/~shadow/sql/sql1992.txt
        [1]: https://docs.microsoft.com/en-us/sql/t-sql/statements/set-quoted-identifier-transact-sql
        [2]: https://www.sqlite.org/lang_keywords.html
      */
      if ($this->__mysqlAnsiQuotesDisabled === null) {
        $this->__mysqlFetchAnsiQuotesDisabled();
      }
      if ($this->__mysqlAnsiQuotesDisabled === true) {
        return '`' . str_replace('`', '``', $ident) . '`';
      }
    }

    return '"' . str_replace('"', '""', $ident) . '"';
  }

  private function __mysqlFetchAnsiQuotesDisabled()
  {
    $q = $this->prepare('SELECT @@SQL_MODE');
    if ($q === false) {
      // neat, a security failure!
      throw new DatabaseException($this->errorInfo());
    }

    $ok = $q->execute();
    if ($ok === false) throw new DatabaseException($q->errorInfo());

    $result = $q->fetch(PDO::FETCH_NUM);
    $this->__mysqlAnsiQuotesDisabled =
        strpos('ANSI_QUOTES', $result[0]) === false;
  }
}
