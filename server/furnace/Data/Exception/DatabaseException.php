<?php

namespace Furnace\Data\Exception;

class DatabaseException extends \Exception
{
  public function __construct(array $errorInfo)
  {
    parent::__construct(
        sprintf('%s (SQLSTATE %s)', $errorInfo[2], $errorInfo[0]));
  }
}
