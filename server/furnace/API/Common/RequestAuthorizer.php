<?php

namespace Furnace\API\Common;

use Symfony\Component\HttpFoundation\Request;

use Pimple\Container;

use Furnace\API\Common\Exception\RequestAuthorizationException as AuthException;
use Furnace\API\Common\Model\Token as APIToken;
use Furnace\API\Common\Model\User as APIUser;
use Furnace\Data\Exception\DatabaseException;
use Furnace\Utilities\Base32;
use Furnace\Utilities\Exception\Base32Exception;

class RequestAuthorizer
{
  protected $globalContainer;
  protected $apiContainer;

  public function __construct(Container $c)
  {
    $this->globalContainer = $c;
    $this->apiContainer = $c['Subcontainers.API'];
  }

  public function authorizeRequest(Request $req)
  {
    if ( ! $req->headers->has('Authorization') ) {
      throw new AuthException();
    }

    $auth_string = $req->headers->get('Authorization');
    list($auth_type, $token) = explode(' ', $auth_string, 2);
    if ($auth_type !== 'Bearer') {
      throw new AuthException();
    }

    try {
      $token_hex = Base32::toHex($token);
    } catch (Base32Exception $e) {
      throw new AuthException();
    }
    if (strlen($token_hex) !== 80) {
      throw new AuthException();
    }
    list($token_sel, $token_val) = str_split($token_hex, 40);

    $db = $this->globalContainer['Database'];
    $q = $db->prepareAndExecute('SELECT id, validator FROM tokens ' .
      'WHERE selector = :selector AND ' .
      ':now < expires_at',
      [ ':selector' => $token_sel, ':now' => time() ],
      [ ':now' => \PDO::PARAM_INT ]
    );
    $result = $q->fetch(\PDO::FETCH_ASSOC);

    if ($result === false) {
      throw new AuthException();
    }

    $token_val_hash = hash('sha256', $token_val);
    if ( ! hash_equals($result['validator'], $token_val_hash)) {
      throw new AuthException();
    }

    return APIToken::fromDatabase($db, $result['id']);
  }

  public function generateToken(APIUser $tokenOwner, DateTime $expiryDate)
  {
    $token_selector = bin2hex( random_bytes(20) );
    $token_validator = bin2hex( random_bytes(20) );

    $db = $this->globalContainer['Database'];
    $db->prepareAndExecute('INSERT INTO tokens ' .
      '( validator,  selector,  expires_at,  owner_id) VALUES ' .
      '(:validator, :selector, :expires_at, :owner_id)',
      [
        ':validator'   => hash('sha256', $token_validator),
        ':selector'    => $token_selector,
        ':expires_at'  => $expiryDate->getTimestamp(),
        ':owner_id'    => $tokenOwner->get('id'),
      ],
      [ ':expires_at' => PDO::PARAM_INT ]
    );

    return Base32::fromHex($token_selector . $token_validator);
  }
}
