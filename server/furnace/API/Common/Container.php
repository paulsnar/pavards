<?php

namespace Furnace\API\Common;

use Pimple\Container as PimpleContainer;

class Container extends PimpleContainer
{
  protected $globalContainer;

  public function __construct(PimpleContainer $c)
  {
    parent::__construct();
    $this->globalContainer = $c;
    $this->injectEntries();
  }

  protected function injectEntries()
  {
    $this['RequestAuthorizer'] = function ($c) {
      return new RequestAuthorizer($this->globalContainer);
    };
  }
}
