<?php

namespace Furnace\API\Common\Model;

use Furnace\Data\Model as DataModel;
use Furnace\Data\DatabaseConnection as DBConn;

class Token extends DataModel
{
  public static function fromDatabase(DBConn $db, $id)
  {
    $t = new Token($db);
    $t->load(['expires_at'], $id);
    return $t;
  }

  static $table = 'tokens';
}
