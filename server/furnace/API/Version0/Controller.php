<?php

namespace Furnace\API\Version0;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

use Pimple\Container;

use Furnace\Exception\JSONSerializationException;
use Furnace\API\Version0\Event\Subscriber as APIEventSubscriber;
use Furnace\API\Common\Exception\RequestAuthorizationException;

class Controller
{
  protected $globalContainer;
  protected $apiContainer;
  protected $routes;
  protected $dispatcher;

  public function __construct(Container $c)
  {
    $this->globalContainer = $c;
    $this->apiContainer = $c['Subcontainers.API'];
    $this->dispatcher = $c['Dispatcher'];

    $this->routes = $this->createRouteCollection();

    $this->dispatcher->addSubscriber(new APIEventSubscriber());
  }

  protected function createRouteCollection()
  {
    $routes = new RouteCollection();

    $routes->add('switches', new Route('/switches', [
      '_controller' => [$this, 'switchesAction'],
    ]));

    $routes->addPrefix('/api/v0');
    $routes->addDefaults([
      '_prefilter' => [$this, 'filterApiRequests'],
    ]);

    return $routes;
  }

  public function getRoutes() { return $this->routes; }

  public function filterApiRequests(Request $req)
  {
    $auth = $this->apiContainer['RequestAuthorizer'];
    try {
      $token = $auth->authorizeRequest($req);
    } catch (RequestAuthorizationException $e) {
      return self::wrapJson(
        [ 'ok' => false, 'error' => 'access_denied' ],
        Response::HTTP_FORBIDDEN);
    }

    $req->attributes->set('api.token', $token);

    return null;
  }

  public static function wrapJson(array $data, int $code = Response::HTTP_OK)
  {
    $r = new Response();
    $r->setStatusCode($code);

    $content = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT);
    if ($content === false) {
      $failure = json_last_error_msg();
      if ($failure === false) {
        // ??
        $failure = '(JSON double internal failure)';
      }
      throw new JSONSerializationException($failure);
    }

    $r->setContent($content . "\n");
    $r->headers->set('Content-Type', 'application/json');
    $r->setCharset('UTF-8');

    return $r;
  }

  public function switchesAction(Request $request)
  {
    return self::wrapJson([]);
  }

}
