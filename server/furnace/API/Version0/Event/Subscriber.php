<?php

namespace Furnace\API\Version0\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

use Furnace\API\Version0\Controller;
use Furnace\API\Version0\Exceptions\AccessDeniedException;

class Subscriber implements EventSubscriberInterface
{
  public static function getSubscribedEvents()
  {
    return [
      KernelEvents::EXCEPTION => [
        'onException',
      ],
    ];
  }

  public function onException(GetResponseForExceptionEvent $e)
  {
    $exc = $e->getException();
    if ($exc instanceof AccessDeniedException) {
      $e->stopPropagation();
      $resp = Controller::wrapJson([
        'ok' => false,
        'error' => 'access_denied',
      ], Response::HTTP_FORBIDDEN);
      $e->setResponse($resp);
    }
  }
}
