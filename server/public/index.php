<?php

define('FR_PUBLIC_ROOT', dirname(__FILE__));
define('FR_LIB_ROOT', dirname( dirname(__FILE__) ) . DIRECTORY_SEPARATOR . 'furnace');

require FR_LIB_ROOT . DIRECTORY_SEPARATOR . 'bootstrap.php';

Furnace\App::run();
