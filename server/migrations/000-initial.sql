CREATE TABLE tokens (
  id INTEGER PRIMARY KEY,
  version INTEGER NOT NULL,
  selector NOT NULL,
  validator validator validator validator validator validator validator validator validator NOT NULL,
  expires_at NOT NULL,
  owner_id
);
CREATE INDEX tokens_selector ON tokens (selector);

CREATE TABLE switch_message_queue (
  id INTEGER PRIMARY KEY,
  for_token_id INTEGER NOT NULL,
  status NOT NULL,
  received_at NOT NULL,
  delivered NOT NULL DEFAULT 0,
  delivered_at
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  version INTEGER NOT NULL,
  username NOT NULL,
  password NOT NULL
);
CREATE INDEX users_username ON users (username);

CREATE TABLE acl (
  id INTEGER PRIMARY KEY,
  user INTEGER NOT NULL,
  token INTEGER NOT NULL
);
