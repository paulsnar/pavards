/*
 * Furnace GPIO daemon
 *
 * © 2017 paulsnar <paulsnar@paulsnar.lv>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License in the LICENSE.txt file in the parent
 * directory, in /usr/share/licenses/common/Apache/license.txt or by visiting
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Note that this code works under the following assumptions:
 * - MCP3008 is connected to the equivalent of spidev0.0 (BCM 8 -> CS,
 *   BCM 9 -> MISO, BCM 10 -> MOSI, BCM 11 -> SCLK)
 * - MAX6955 is connected to the equivalent of i2c-1 (BCM 2 -> SDA,
 *   BCM 3 -> SCL)
 * - Only BCM 17, 22 and 27 pins can be set on/off arbitrarily.
 */

/* Config variables. */

#define COMMS_BIND_PORT     5588  // via UDP
#define RUN_AS_USER         65534 // nobody
#define RUN_AS_GROUP        65534 // nogroup
#define MAX6955_ADDRESS     0x60  // AD0, AD1 -> GND

/* End config variables. */

#define _GNU_SOURCE

#ifdef __clang__
#pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments" // :/
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>

#include <sys/epoll.h>
#include <sys/mman.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <sched.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <bcm2835.h>

#define IPV6 0 // not quite implemented yet!

#define __LOG(type, fmt, ...) printf("[" type "] " fmt "\n", ##__VA_ARGS__)

#ifdef _DEBUG_LOG
#define LOG_DEBUG(fmt, ...) __LOG("debug", fmt, ##__VA_ARGS__)
#else
#define LOG_DEBUG(...) do {} while (0)
#endif

#define LOG_INFO(fmt, ...) __LOG("info", fmt, ##__VA_ARGS__)
#define LOG_WARN(fmt, ...) __LOG("warn", fmt, ##__VA_ARGS__)
#define LOG_FATAL(fmt, ...) __LOG("fatal", fmt, ##__VA_ARGS__)
// #define LOG_FATAL_ERR() LOG_FATAL("Error: %s", strerror(errno))
#define LOG_FATAL_ERR_CTX(ctx) \
  LOG_FATAL("Error in %s: %i %s", ctx, errno, strerror(errno))

#define WARN_ERR_CTX(ctx) \
  LOG_WARN("Notice in %s: %i %s", ctx, errno, strerror(errno))
#define FAIL_ERR_CTX(ctx) \
  do { LOG_FATAL_ERR_CTX(ctx); goto fail_nolog; } while (0)

typedef int fd_t;

#define CMD_PING          '?'
#define CMD_VERSION       'V'
#define CMD_MCP3008_READ  'i'
#define CMD_MAX6955_READ  'r'
#define CMD_MAX6955_WRITE 'R'
#define CMD_PIN_SET       ':'

#define RSP_CONTENT_SHORT '|'
#define RSP_CONTENT_LONG  '}'
#define RSP_NO_CONTENT    '~'
#define RSP_ERROR         '!'

#define CMERR_NOIMPL  0x28
#define CMERR_GPIO    0x29
#define CMERR_BADRQ   0x30
#define CMERR_REFUSED 0x31

#define COMMS_PROTO_VERSION   0x01
#define COMMS_MAX_MSG_LENGTH  259

#define neuter(ident) do { free(ident); ident = NULL; } while (0)

#define TRY_SEND(sockfd, buf, length, flags, dest, dest_len) \
  if (sendto(sockfd, buf, length, flags, dest, dest_len) < 0) \
    WARN_ERR_CTX("socket response")
#define ASSERT_M(c, m) if (c == -1) { LOG_FATAL_ERR_CTX(m); return -1; }

typedef unsigned char byte;
static socklen_t addr_size = sizeof(struct sockaddr_in);

int m_handle_datagram(fd_t socket) {
  byte incoming[COMMS_MAX_MSG_LENGTH], response[COMMS_MAX_MSG_LENGTH];
  memset(incoming, 0, COMMS_MAX_MSG_LENGTH);
  memset(response, 0, COMMS_MAX_MSG_LENGTH);

  // SOMEDAY: make this work with IPv6
#if IPV6
#error "This part is not implemented for IPv6 yet. FIXME?"
#endif
  struct sockaddr_in client;
  memset(&client, 0, sizeof(struct sockaddr_in));

  ASSERT_M(
    recvfrom(socket, incoming, COMMS_MAX_MSG_LENGTH, 0, &client, &addr_size),
    "datagram receive");

  response[0] = incoming[0]; // sequence counter

  // reject connections from non-local hosts
  // i.e. IP doesn't belong to the 127.0.0.0/8 subnet
  // i.e. IP first octet is not 127
  if ((ntohl(client.sin_addr.s_addr) >> 24) ^ 127) {
    response[1] = RSP_ERROR;
    response[2] = CMERR_REFUSED;
    TRY_SEND(socket, response, 3, 0, &client, addr_size);
    return 0;
  }

  unsigned char command = incoming[1];
  char gpio_buf[16];
  memset(gpio_buf, 0, 16);
  uint8_t bcm_xfer_result;

  switch (command) {
  case CMD_PING:
    response[1] = RSP_NO_CONTENT;
    response[2] = incoming[2];
    TRY_SEND(socket, response, 3, 0, &client, addr_size);
    return 0;

  case CMD_VERSION:
    response[1] = RSP_NO_CONTENT;
    response[2] = COMMS_PROTO_VERSION;
    TRY_SEND(socket, response, 3, 0, &client, addr_size);
    return 0;

  case CMD_MCP3008_READ:
    // response:
    // +0      +1      +2      +3      +4      +5
    // [seq  ] [ "}" ] [0x01 ] [byte1] [byte2] [...  ]
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);

    response[5] = 1;
    response[6] = 0x80 | (incoming[2] << 5);
    bcm2835_spi_transfern((char*) response + 5, 3);

    response[1] = RSP_CONTENT_LONG;
    response[2] = 1;
    response[3] = response[6] & 7;
    response[4] = response[7];
    TRY_SEND(socket, response, 6, 0, &client, addr_size);

    // pull CS0 to high again
    bcm2835_spi_chipSelect(BCM2835_SPI_CS1);
    bcm2835_spi_transfer(0);
    return 0;

  case CMD_MAX6955_READ:
    // incoming:
    // [seq  ] [cmd  ] [freg ] [bytec]
    bcm2835_i2c_setSlaveAddress(MAX6955_ADDRESS);
    bcm_xfer_result = bcm2835_i2c_read_register_rs(
      (char*) incoming + 2, (char*) response + 3, incoming[3]);
    if (bcm_xfer_result != BCM2835_I2C_REASON_OK) {
      LOG_WARN("I2C read failed: %i", bcm_xfer_result);
      response[1] = RSP_ERROR;
      response[2] = CMERR_GPIO;
      TRY_SEND(socket, response, 3, 0, &client, addr_size);
    } else {
      response[1] = RSP_CONTENT_SHORT;
      response[2] = incoming[3];
      TRY_SEND(socket, response, incoming[3] + 3, 0, &client, addr_size);
    }
    return 0;

  case CMD_MAX6955_WRITE:
    bcm2835_i2c_setSlaveAddress(MAX6955_ADDRESS);
    /* For ease of transfer and simpler code (and less memory usage), we reuse
     * the incoming buffer as the write buffer for the I2C transfer. However,
     * the bytes are ordered incorrectly, so we discard the command byte and
     * instead shift register and bytecount bytes one position left and then
     * put the command byte right before the actual data, so we can use the
     * BCM2835 I2C write facilities with the buffer we received. Handy!
     */
                                // [seq  ] [cmd  ] [freg ] [bytec] data...
    incoming[1] = incoming[2];  // [seq  ] [freg ] [freg ] [bytec] data...
    incoming[2] = incoming[3];  // [seq  ] [freg ] [bytec] [bytec] data...
    incoming[3] = incoming[1];  // [seq  ] [freg ] [bytec] [freg ] data...
    // we just write the buffer as is starting from here  ^
    bcm_xfer_result = bcm2835_i2c_write((char*) incoming + 3, incoming[2] + 1);
    if (bcm_xfer_result != BCM2835_I2C_REASON_OK) {
      LOG_WARN("I2C write failed: %i", bcm_xfer_result);
      response[1] = RSP_ERROR;
      response[2] = CMERR_GPIO;
    } else {
      response[1] = RSP_NO_CONTENT;
      response[2] = incoming[2];
    }
    TRY_SEND(socket, response, 3, 0, &client, addr_size);
    return 0;

  case CMD_PIN_SET:
    switch (incoming[2]) {
    case 17:
    case 22:
    case 27:
      if (incoming[3]) {
        bcm2835_gpio_set(incoming[2]);
      } else {
        bcm2835_gpio_clr(incoming[2]);
      }
      response[1] = RSP_NO_CONTENT;
      response[2] = incoming[3];
      TRY_SEND(socket, response, 3, 0, &client, addr_size);
      return 0;

    default:
      response[1] = RSP_ERROR;
      response[2] = CMERR_BADRQ;
      TRY_SEND(socket, response, 3, 0, &client, addr_size);
      return 0;
    }

  default:
    response[1] = RSP_ERROR;
    response[2] = CMERR_NOIMPL;
    TRY_SEND(socket, response, 3, 0, &client, addr_size);
    return 0;
  }
}

#define ASSERT(c, m) if (c == -1) FAIL_ERR_CTX(m)

int main(void) {
  fd_t sockfd = -1, sigfd = -1, epollfd = -1;
  char bcm_initmask = 0;

  // Set up realtime mode
  struct sched_param sp;
  memset(&sp, 0, sizeof(struct sched_param));
  sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
  ASSERT(sched_setscheduler(0, SCHED_FIFO, &sp), "sched");
  mlockall(MCL_CURRENT | MCL_FUTURE);

  // Open communication socket
#if IPV6
  sockfd = socket(AF_INET6, SOCK_DGRAM, 0);
#else
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
#endif
  ASSERT(sockfd, "sockfd open");

  struct addrinfo *addr, hints;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG | AI_NUMERICSERV;

  char port_str[6]; sprintf(port_str, "%d", COMMS_BIND_PORT);
  getaddrinfo(NULL, port_str, &hints, &addr);

  struct addrinfo *rp;

  for (rp = addr; rp != NULL; rp = rp->ai_next) {
    sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    ASSERT(sockfd, "sockfd open");

    if (bind(sockfd, rp->ai_addr, rp->ai_addrlen) == 0) break;

    close(sockfd);
    if (rp->ai_next == NULL) {
      sockfd = -1;
      break;
    }
  }
  ASSERT(sockfd, "address selection");

  char *inet_addr_str = malloc(64);
  memset(inet_addr_str, 0, 64);
  inet_ntop(rp->ai_family, rp->ai_addr, inet_addr_str, 64);
  LOG_INFO("Listening on %s:%d", inet_addr_str,
    ntohs(((struct sockaddr_in*) rp->ai_addr)->sin_port));
  neuter(inet_addr_str);
  freeaddrinfo(addr);


  // Set up GPIO
  if ( ! bcm2835_init()) FAIL_ERR_CTX("bcm2835 init");
    else bcm_initmask |= 0x1;
  if ( ! bcm2835_i2c_begin()) FAIL_ERR_CTX("bcm2835 i2c init");
    else bcm_initmask |= 0x2;
  if ( ! bcm2835_spi_begin()) FAIL_ERR_CTX("bcm2835 spi init");
    else bcm_initmask |= 0x4;
  bcm2835_gpio_fsel(17, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(22, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(27, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, 0);
  bcm2835_spi_setDataMode(BCM2835_SPI_MODE1);

  // Make sure CS0 is set to high to power off MCP3008
  bcm2835_spi_chipSelect(BCM2835_SPI_CS1);
  bcm2835_spi_transfer(0x00);

  // Drop privileges
  char privilege_drop_result = 0;
  privilege_drop_result |= setregid(RUN_AS_GROUP, RUN_AS_GROUP);
  privilege_drop_result |= setreuid(RUN_AS_USER,  RUN_AS_USER);
  if (privilege_drop_result != 0) {
    WARN_ERR_CTX("privilege drop");
    LOG_WARN("Continuing as running user. This might be dangerous!%s", "");
  }

  // Set up signal handling
  sigset_t sigfd_handled;
  sigemptyset(&sigfd_handled);
  sigaddset(&sigfd_handled, SIGINT);
  sigaddset(&sigfd_handled, SIGQUIT);
  sigaddset(&sigfd_handled, SIGTERM);
  pthread_sigmask(SIG_BLOCK, &sigfd_handled, NULL);
  sigfd = signalfd(-1, &sigfd_handled, 0);

  // Set up the epoll
  epollfd = epoll_create1(0);
  ASSERT(epollfd, "epoll create");

  struct epoll_event ev;
  memset(&ev, 0, sizeof(struct epoll_event));
  ev.events = EPOLLIN;

  ev.data.fd = sockfd;
  ASSERT(epoll_ctl(epollfd, EPOLL_CTL_ADD, sockfd, &ev), "epoll add sockfd");

  ev.data.fd = sigfd;
  ASSERT(epoll_ctl(epollfd, EPOLL_CTL_ADD, sigfd, &ev), "epoll add sigfd");

  memset(&ev, 0, sizeof(struct epoll_event));

  struct signalfd_siginfo siginfo;
  memset(&siginfo, 0, sizeof(struct signalfd_siginfo));

  int ready_fds;

  for (;;) {
    ready_fds = epoll_wait(epollfd, &ev, 1, -1);
    if (ready_fds == -1 && errno == EINTR) continue; // pff
    ASSERT(ready_fds, "epoll loop");

    if (ev.data.fd == sigfd) {
      ASSERT(
        read(sigfd, &siginfo, sizeof(struct signalfd_siginfo)), "sigfd read");
      LOG_DEBUG("handling signal %i...", siginfo.ssi_signo);
      goto cleanup;
    }

    if (ev.data.fd == sockfd) {
      if (m_handle_datagram(sockfd) == -1) goto fail_nolog;
    }
  }

// fail:
//   LOG_FATAL_ERR();

fail_nolog:
cleanup:
  if (epollfd != -1) close(epollfd);
  if (sigfd != -1) close(sigfd);
  if (sockfd != -1) close(sockfd);

  if (bcm_initmask & 0x4) bcm2835_spi_end();
  if (bcm_initmask & 0x2) bcm2835_i2c_end();
  if (bcm_initmask & 0x1) bcm2835_close();

  if (errno == 0) return 0;
  else exit(EXIT_FAILURE);
}
