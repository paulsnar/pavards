Furnace GPIO daemon
-------------------

Motivation
==========
Since in order to successfully communicate with external devices via the GPIO
pins the program must be run with superuser privileges, and I do not feel
comfortable running the entirety of the daemon with superuser privileges, I
decided to break out the communications into a separate daemon. This is it.


Usage
=====
 1. Run `makepkg` in current directory. A Pacman package file will be generated.
    It can be used as an argument for `pacman -U` to install gpiod as a global
    package.
 2. Perform `systemctl enable furnace-gpiod`.
 3. Either perform a reboot or `systemctl start furnace-gpiod`.
 4. Communicate with it over UDP using the protocol described in PROTOCOL.txt.


Notes
=====
This daemon is made for and currently (likely) works only on Arch Linux and
on Raspberry Pi (but might work on other BCM2835-based boards as well).
